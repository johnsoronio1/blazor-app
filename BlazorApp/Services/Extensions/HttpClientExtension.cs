﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BlazorApp.Services.Extensions
{
    public static class HttpClientExtension
    {
        public static async Task<HttpResponseMessage> GetJsonAsync(this HttpClient httpClient, string url, AuthenticationHeaderValue authorization)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = authorization;
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "localhost");
            request.Headers.Add("Cache-Control", "no-cache");
            request.Headers.Add("Connection", "keep-alive");

            return await httpClient.SendAsync(request);
        }

        public static async Task<HttpResponseMessage> PostJsonAsync(this HttpClient httpClient, string url, string jsonString, AuthenticationHeaderValue authorization)
        {
            var requestMessage = new HttpRequestMessage {
                Content = new StringContent(jsonString, Encoding.UTF8, "application/json")
            };

            httpClient.DefaultRequestHeaders.Authorization = authorization;
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("User-Agent", "localhost");
            httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
            httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");

            return await httpClient.PostAsync(url, requestMessage.Content);
        }

        public static async Task<HttpResponseMessage> PutJsonAsync(this HttpClient httpClient, string url, string jsonString, AuthenticationHeaderValue authorization)
        {
            var requestMessage = new HttpRequestMessage
            {
                Content = new StringContent(jsonString, Encoding.UTF8, "application/json")
            };

            httpClient.DefaultRequestHeaders.Authorization = authorization;
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("User-Agent", "localhost");
            httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
            httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");

            return await httpClient.PutAsync(url, requestMessage.Content);
        }

        public static async Task<HttpResponseMessage> DeleteAsync(this HttpClient httpClient, string url, AuthenticationHeaderValue authorization)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url);
            request.Headers.Authorization = authorization;
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "localhost");
            request.Headers.Add("Cache-Control", "no-cache");
            request.Headers.Add("Connection", "keep-alive");

            return await httpClient.SendAsync(request);
        }
    }
}
