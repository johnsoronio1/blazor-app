﻿using BlazorApp.Common;
using BlazorApp.Interfaces;
using BlazorApp.Models;
using BlazorApp.Models.Requests;
using BlazorApp.Models.Responses;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorApp.Services
{
    public class AccountService : IAccountService
    {
        private readonly HttpClient _http;
        private readonly IConfiguration _config;
        private readonly string _apiServerEndpoint;

        public AccountService(HttpClient http, IConfiguration config)
        {
            _http = http;
            _config = config;
            _apiServerEndpoint = $"{_config[Global.ApiEndpoint]}/api";
        }

        public async Task<ApiResponseDto> LoginAsync(Credentials credentials)
        {
            return await _http.PostJsonAsync<ApiResponseDto>($"{_apiServerEndpoint}/accounts/login", credentials);
        }

        public async Task<ApiResponseDto> RegisterAsync(UserEntry entry)
        {
            return await _http.PostJsonAsync<ApiResponseDto>($"{_apiServerEndpoint}/accounts/register", entry);
        }

        public async Task<ApiResponseDto> LogoutAsync(string refreshToken)
        {
            return await _http.GetJsonAsync<ApiResponseDto>($"{_apiServerEndpoint}/accounts/logout?refreshToken={refreshToken}");
        }
    }
}
