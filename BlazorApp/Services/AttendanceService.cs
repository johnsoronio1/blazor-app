﻿using System.Threading.Tasks;
using BlazorApp.Interfaces;
using BlazorApp.Models;
using BlazorApp.Models.Responses;

namespace BlazorApp.Services
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IHttpClientService _httpService;

        public AttendanceService(IHttpClientService httpService)
        {
            _httpService = httpService;
        }

        public async Task<ApiResponseDto> CreateAsync(AttendanceDto model)
        {
            return await _httpService.PostJsonAsync("attendances", model);
        }

        public async Task<ApiResponseDto> DeleteAsync(int id)
        {
            return await _httpService.DeleteJsonAsync($"attendances/{id}");
        }

        public async Task<ApiResponseDto> GetByIdAsync(int id)
        {
            return await _httpService.GetJsonAsync($"attendances/{id}");
        }

        public async Task<ApiResponseDto> GetListAsync()
        {
            return await _httpService.GetJsonAsync("attendances");
        }

        public async Task<ApiResponseDto> UpdateAsync(AttendanceDto model)
        {
            return await _httpService.PutJsonAsync($"attendances/{model.AttendanceId}", model);
        }
    }
}
