﻿using System.Threading.Tasks;
using BlazorApp.Interfaces;
using BlazorApp.Models.Responses;

namespace BlazorApp.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IHttpClientService _httpService;

        public EmployeeService(IHttpClientService httpService)
        {
            _httpService = httpService;
        }

        public async Task<ApiResultDto<EmployeeDto>> CreateAsync(EmployeeDto user)
        {
            return await _httpService.PostJsonAsync("employees", user);
        }

        public async Task<ApiResponseDto> DeleteAsync(int id)
        {
            return await _httpService.DeleteJsonAsync($"employees/{id}");
        }

        public async Task<ApiResponseDto> GetByIdAsync(int id)
        {
            return await _httpService.GetJsonAsync($"employees/{id}");
        }

        public async Task<ApiResponseDto> GetListAsync()
        {
            return await _httpService.GetJsonAsync("employees");
        }

        public async Task<ApiResponseDto> UpdateAsync(EmployeeDto employee)
        {
            return await _httpService.PutJsonAsync($"employees/{employee.EmployeeId}", employee);
        }
    }
}
