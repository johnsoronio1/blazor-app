﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BlazorApp.Common;
using BlazorApp.Interfaces;
using BlazorApp.Models.Responses;
using BlazorApp.Services.Extensions;
using Microsoft.Extensions.Configuration;

namespace BlazorApp.Services
{
    public class HttpClientService : IHttpClientService
    {
        private readonly HttpClient _http;
        private readonly IUserSession _userSession;
        private readonly IConfiguration _configuration;
        private readonly string _apiServerEndpoint;

        public HttpClientService(HttpClient http, IConfiguration configuration, IUserSession userSession)
        {
            _http = http;
            _userSession = userSession;
            _configuration = configuration;
            _apiServerEndpoint = $"{_configuration[Global.ApiEndpoint]}/api";
        }

        public async Task<ApiResponseDto> GetJsonAsync(string path)
        {
            var response = await _http.GetJsonAsync($"{_apiServerEndpoint}/{path}", new AuthenticationHeaderValue(Global.Bearer, _userSession.BearerToken));
            return await BuildResponseDto(response);
        }

        public async Task<ApiResponseDto> PostJsonAsync(string path)
        {
            var response = await _http.PostJsonAsync($"{_apiServerEndpoint}/{path}", string.Empty, new AuthenticationHeaderValue(Global.Bearer, _userSession.BearerToken));
            return await BuildResponseDto(response);
        }

        public async Task<ApiResponseDto> PostJsonAsync<T>(string path, T formProperties) where T : new()
        {
            var body = JsonConvert.SerializeObject(formProperties);
            var response = await _http.PostJsonAsync($"{_apiServerEndpoint}/{path}", body, new AuthenticationHeaderValue(Global.Bearer, _userSession.BearerToken));
            return await BuildResponseDto(response);
        }

        public async Task<ApiResponseDto> PutJsonAsync<T>(string path, T formProperties) where T : new()
        {
            var body = JsonConvert.SerializeObject(formProperties);
            var response = await _http.PutJsonAsync($"{_apiServerEndpoint}/{path}", body, new AuthenticationHeaderValue(Global.Bearer, _userSession.BearerToken));
            return await BuildResponseDto(response);
        }

        public async Task<ApiResponseDto> PutJsonAsync(string path)
        {
            var response = await _http.PutJsonAsync($"{_apiServerEndpoint}/{path}", string.Empty, new AuthenticationHeaderValue(Global.Bearer, _userSession.BearerToken));
            return await BuildResponseDto(response);
        }

        public async Task<ApiResponseDto> DeleteJsonAsync(string path)
        {
            var response = await _http.DeleteAsync($"{_apiServerEndpoint}/{path}", new AuthenticationHeaderValue(Global.Bearer, _userSession.BearerToken));
            return await BuildResponseDto(response);
        }

        private async Task<ApiResponseDto> BuildResponseDto(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();
            var responseDto = new ApiResponseDto();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                responseDto.IsSuccess = true;
                responseDto.JsonString = content;
            }
            else
            {
                responseDto = JsonConvert.DeserializeObject<ApiResponseDto>(content);
                responseDto.IsSuccess = false;
            }
            return responseDto;
        }
    }
}
