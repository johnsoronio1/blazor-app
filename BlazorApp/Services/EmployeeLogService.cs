﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Interfaces;
using BlazorApp.Models.Responses;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace BlazorApp.Services
{
    public class EmployeeLogService : IEmployeeLogService
    {
        private readonly IHttpClientService _httpService;
        private readonly IMapper _mapper;

        public EmployeeLogService(IMapper mapper, IHttpClientService httpService)
        {
            _mapper = mapper;
            _httpService = httpService;
        }

        public async Task<ApiResultDto<LogDto>> CreateAsync(LogDto model)
        {
            var response = await _httpService.PostJsonAsync("employeeLogs", model);
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<LogDto>>(response);
            if (response.IsSuccess)
            {
                result.Data = JsonConvert.DeserializeObject<LogDto>(response.JsonString);
            }
            return result;
        }

        public async Task<ApiResultDto<bool>> DeleteAsync(int id)
        {
            var response = await _httpService.DeleteJsonAsync($"employeeLogs/{id}");
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<bool>>(response);
            if (response.Status == StatusCodes.Status200OK)
            {
                result.Data = JsonConvert.DeserializeObject<bool>(response.JsonString);
            }
            return result;
        }

        public async Task<ApiResultDto<LogDto>> GetByIdAsync(int id)
        {
            var response = await _httpService.GetJsonAsync($"employeeLogs/{id}");
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<LogDto>>(response);
            if (response.Status == StatusCodes.Status200OK)
            {
                result.Data = JsonConvert.DeserializeObject<LogDto>(response.JsonString);
            }
            return result;
        }

        public async Task<ApiResultDto<List<LogDto>>> GetListAsync()
        {
            var response = await _httpService.GetJsonAsync("employeeLogs");
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<List<LogDto>>>(response);
            if (response.Status == StatusCodes.Status200OK)
            {
                result.Data = JsonConvert.DeserializeObject<List<LogDto>>(response.JsonString);
            }
            return result;
        }

        public async Task<ApiResultDto<List<LogDto>>> GetLogsByEmployee()
        {
            var response = await _httpService.GetJsonAsync("employeeLogs/current");
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<List<LogDto>>>(response);
            if (response.Status == StatusCodes.Status200OK)
            {
                result.Data = JsonConvert.DeserializeObject<List<LogDto>>(response.JsonString);
            }
            return result;
        }

        public async Task<ApiResultDto<bool>> TimeIn()
        {
            var response = await _httpService.PostJsonAsync("employeeLogs/timein");
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<bool>>(response);
            if (response.Status == StatusCodes.Status200OK)
            {
                result.Data = JsonConvert.DeserializeObject<bool>(response.JsonString);
            }
            return result;
        }

        public async Task<ApiResponseDto> TimeOUT()
        {
            var response = await _httpService.PostJsonAsync("employeeLogs/timeout");
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<bool>>(response);
            if (response.Status == StatusCodes.Status200OK)
            {
                result.Data = JsonConvert.DeserializeObject<bool>(response.JsonString);
            }
            return result;
        }

        public async Task<ApiResultDto<LogDto>> UpdateAsync(LogDto model)
        {
            var response = await _httpService.PutJsonAsync($"employeeLogs/{model.LogId}", model);
            var result = _mapper.Map<ApiResponseDto, ApiResultDto<LogDto>>(response);
            if (response.Status == StatusCodes.Status200OK)
            {
                result.Data = JsonConvert.DeserializeObject<LogDto>(response.JsonString);
            }
            return result;
        }
    }
}
