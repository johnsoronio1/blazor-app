﻿using System.Threading.Tasks;
using BlazorApp.Interfaces;
using BlazorApp.Models.Requests;
using BlazorApp.Models.Responses;

namespace BlazorApp.Services
{
    public class UserService : IUserService
    {
        private readonly IHttpClientService _httpService;

        public UserService(IHttpClientService httpService)
        {
            _httpService = httpService;
        }

        public async Task<ApiResponseDto> GetList()
        {
            return await _httpService.GetJsonAsync("users");
        }

        public async Task<ApiResponseDto> Create(UserEntry user)
        {
            return await _httpService.PostJsonAsync("users", user);
        }

        public async Task<ApiResponseDto> GetById(int id)
        {
            return await _httpService.GetJsonAsync($"users/{id}");
        }

        public async Task<ApiResponseDto> Update(UserEditModel user)
        {
            return await _httpService.PutJsonAsync($"users/{user.Id}", user);
        }

        public async Task<ApiResponseDto> Delete(int id)
        {
            return await _httpService.DeleteJsonAsync($"users/{id}");
        }
    }
}
