﻿using System.Threading.Tasks;
using BlazorApp.Interfaces;
using BlazorApp.Models.Responses;

namespace BlazorApp.Services
{
    public class HolidayService : IHolidayService
    {
        private readonly IHttpClientService _httpService;

        public HolidayService(IHttpClientService httpService)
        {
            _httpService = httpService;
        }

        public async Task<ApiResponseDto> CreateAsync(HolidayDto model)
        {
            return await _httpService.PostJsonAsync("holidays", model);
        }

        public async Task<ApiResponseDto> DeleteAsync(int id)
        {
            return await _httpService.DeleteJsonAsync($"holidays/{id}");
        }

        public async Task<ApiResponseDto> GetByIdAsync(int id)
        {
            return await _httpService.GetJsonAsync($"holidays/{id}");
        }

        public async Task<ApiResponseDto> GetListAsync()
        {
            return await _httpService.GetJsonAsync("holidays");
        }

        public async Task<ApiResponseDto> UpdateAsync(HolidayDto model)
        {
            return await _httpService.PutJsonAsync($"holidays/{model.HolidayId}", model);
        }
    }
}
