﻿using System.Threading.Tasks;
using BlazorApp.Interfaces;
using BlazorApp.Models.Responses;

namespace BlazorApp.Services
{
    public class WorkShiftService : IWorkShiftService
    {
        private readonly IHttpClientService _httpService;

        public WorkShiftService(IHttpClientService httpService)
        {
            _httpService = httpService;
        }

        public async Task<ApiResponseDto> CreateAsync(WorkShiftDto model)
        {
            return await _httpService.PostJsonAsync("workshifts", model);
        }

        public async Task<ApiResponseDto> DeleteAsync(int id)
        {
            return await _httpService.DeleteJsonAsync($"workshifts/{id}");
        }

        public async Task<ApiResponseDto> GetByIdAsync(int id)
        {
            return await _httpService.GetJsonAsync($"workshifts/{id}");
        }

        public async Task<ApiResponseDto> GetListAsync()
        {
            return await _httpService.GetJsonAsync("workshifts");
        }

        public async Task<ApiResponseDto> UpdateAsync(WorkShiftDto model)
        {
            return await _httpService.PutJsonAsync($"workshifts/{model.WorkShiftId}", model);
        }
    }
}
