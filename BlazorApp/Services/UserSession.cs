﻿using System;
using System.Security.Claims;
using BlazorApp.Common;
using BlazorApp.Interfaces;
using Microsoft.AspNetCore.Http;

namespace BlazorApp.Services
{
    public class UserSession : IUserSession
    {
        private readonly HttpContext _context;
        public UserSession(IHttpContextAccessor httpContextAccessor)
        {
            _context = httpContextAccessor.HttpContext;
        }

        public int UserId
        {
            get { return Convert.ToInt32(_context.User.FindFirst(ClaimTypes.NameIdentifier).Value); }
        }
        public string Username
        {
            get { return _context.User.FindFirst(ClaimTypes.Name).Value; }
        }
        public string BearerToken
        {
            get { return _context.User.FindFirst(Global.AccessToken).Value; }
        }
        public string DisplayName
        {
            get { return _context.User.FindFirst(ClaimTypes.GivenName).Value; }
        }
        public bool IsAuthenticated
        {
            get { return _context.User.Identity.IsAuthenticated;  }
        }
    }
}
