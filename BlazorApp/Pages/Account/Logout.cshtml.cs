﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Interfaces;
using BlazorApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BlazorApp.Pages.Account
{
    [Authorize]
    public class LogoutModel : PageModel
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<LoginModel> _logger;
        private readonly IAccountService _accountService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public LogoutModel(ILogger<LoginModel> logger, IHttpContextAccessor httpContextAccessor, IConfiguration configuration, IAccountService accountService)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _accountService = accountService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var token = _session.GetString("authToken");
            
            if (!string.IsNullOrEmpty(token)) 
            {
                var authToken = JsonConvert.DeserializeObject<LoginResult>(token);

                if (!string.IsNullOrEmpty(authToken.RefreshToken))
                {
                    await _accountService.LogoutAsync(authToken.RefreshToken);
                    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                    return LocalRedirect(Url.Content("~/"));
                }
            }

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return LocalRedirect(Url.Content("~/login"));
        }
    }
}