﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using BlazorApp.Common;
using BlazorApp.Interfaces;
using BlazorApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace BlazorApp.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly ILogger<LoginModel> _logger;
        private readonly IAccountService _accountService;

        public LoginModel(ILogger<LoginModel> logger, IAccountService accountService)
        {
            _logger = logger;
            _accountService = accountService;
        }


        [BindProperty]
        public Credentials Input { get; set; }

        [BindProperty]
        public string ReturnUrl { get; set; }

        public void OnGet() 
        {
            ReturnUrl = HttpContext.Request.Query["returnUrl"];
        }

        public async Task<IActionResult> OnPostAsync()
        {
            try
            {
                var response = await _accountService.LoginAsync(Input);

                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrEmpty(response.AccessToken))
                    {
                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.NameIdentifier, response.UserId.ToString()),
                            new Claim(ClaimTypes.Name, Input.Username),
                            new Claim(ClaimTypes.GivenName, response.DisplayName),
                            new Claim(ClaimTypes.Role, RolesEnum.Administrator.ToString()),
                            new Claim(Global.AccessToken, response.AccessToken),
                            new Claim(Global.RefreshToken, response.RefreshToken)
                        };

                        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                        
                        if(string.IsNullOrEmpty(ReturnUrl))
                            return LocalRedirect(Url.Content("~/"));
                        else
                            return LocalRedirect(Url.Content("~/") + ReturnUrl);
                    }
                }
            }
            catch (WebException webEx)
            {
                _logger.LogError("WebException occured in Submit Login", webEx);
            }
            catch (Exception ex)
            {
                _logger.LogError("Excepton occured in Submit Login ", ex);
            }

            ModelState.AddModelError(string.Empty, "Username or password is incorrect.");

            return Page();
        }
    }
}