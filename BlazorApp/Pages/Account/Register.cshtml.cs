﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using BlazorApp.Interfaces;
using BlazorApp.Models;
using BlazorApp.Models.Requests;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BlazorApp.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<LoginModel> _logger;
        private readonly IAccountService _accountService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public RegisterModel(ILogger<LoginModel> logger, IHttpContextAccessor httpContextAccessor, IConfiguration configuration, IAccountService accountService)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _accountService = accountService;
        }


        [BindProperty]
        public RegisterEntryModel Input { get; set; }

        public bool ShowValidationSummary { get; set; }

        public void OnGet()
        {
            ShowValidationSummary = false;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            ApiResponseDto response = new ApiResponseDto();
            ShowValidationSummary = false;

            try
            {
                if (ModelState.IsValid)
                {
                    var userEntry = new UserEntry() 
                    { 
                        Username = Input.Username,
                        Password = Input.NewPassword,
                        DisplayName = Input.DisplayName,
                        RoleType = "Adminstrator"
                    };

                    response = await _accountService.RegisterAsync(userEntry);
                    if (response.StatusCode == StatusCodes.Status200OK)
                    {
                        return LocalRedirect(Url.Content("~/login"));
                    }

                    ModelState.AddModelError(string.Empty, response.Message);
                    ShowValidationSummary = true;
                }
            }
            catch (WebException webEx)
            {
                _logger.LogError("WebException occured in Submit Login", webEx);
            }
            catch (Exception ex)
            {
                _logger.LogError("Excepton occured in Submit Login ", ex);
            }

            return Page();
        }
    }
}