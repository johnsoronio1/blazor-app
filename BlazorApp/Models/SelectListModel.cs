﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class SelectListModel
    {
        public int? Value { get; set; }
        public string Text { get; set; }
    }
}
