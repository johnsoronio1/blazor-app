﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlazorApp.Models.Responses
{
    /// <summary>
    /// The Api Response Dto.
    /// </summary>
    public class ApiResponseDto
    {
        /// <summary>
        /// Gets or sets the error title.
        /// </summary>
        /// <value>
        /// The error title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the model state validation errors.
        /// </summary>
        /// <value>
        /// The model state validation errors.
        /// </value>
        public IEnumerable<ValidationErrorDto> Errors { get; set; }

        /// <summary>
        /// Gets or sets the reference code.
        /// </summary>
        /// <value>
        /// The reference code.
        /// </value>
        public string ReferenceCode { get; set; }

        /// <summary>
        /// Gets or sets the reference document link.
        /// </summary>
        /// <value>
        /// The reference document link.
        /// </value>
        public string ReferenceDocumentLink { get; set; }

        /// <summary>
        /// Gets or sets the success.
        /// </summary>
        /// <value>
        /// The success.
        /// </value>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets the Json String.
        /// </summary>
        /// <value>
        /// The Json String.
        /// </value>
        [JsonIgnore]
        public string JsonString { get; set; }
    }
}
