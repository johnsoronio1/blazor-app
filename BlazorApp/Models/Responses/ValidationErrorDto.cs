﻿using Newtonsoft.Json;

namespace BlazorApp.Models.Responses
{
    public class ValidationErrorDto
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; }
        public string Message { get; }
        public ValidationErrorDto(string field, string message)
        {
            Field = field != string.Empty ? field : null;
            Message = message;
        }
    }
}
