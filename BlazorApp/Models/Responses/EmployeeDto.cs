﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlazorApp.Models.Responses
{
    public class EmployeeDto
    {
        [Display(Name = "Employee Id")]
        public int EmployeeId { get; set; }

        [Display(Name = "User Id")]
        public int? UserId { get; set; }

        public string EmployeeCompanyId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Prefix")]
        public string PrefixName { get; set; }

        [Display(Name = "Suffix")]
        public string SuffixName { get; set; }

        [Required]
        public int? Age { get; set; }

        [Required]
        public string Gender { get; set; }
        public string Active { get; set; }

        [Required]
        [Display(Name = "Birth Date")]
        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; }

        [Required]
        [Display(Name = "Hired Date")]
        [DataType(DataType.Date)]
        public DateTime? HiredDate { get; set; }

    }
}
