﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BlazorApp.Models
{
    public class AttendanceDto
    {
        public int AttendanceId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        public DateTime EffectiveDate { get; set; }
        public bool? IsRestDay { get; set; }

        [JsonIgnore]
        public string IsRestDayString { get; set; }

        public bool? IsHoliday { get; set; }

        [JsonIgnore]
        public string IsHolidayString { get; set; }

        public bool? IsLeave { get; set; }

        [JsonIgnore]
        public string IsLeaveString { get; set; }

        public TimeSpan? TimeIn { get; set; }
        public TimeSpan? TimeOut { get; set; }
        public decimal? TotalWorkHr { get; set; }
        public decimal? TotalLateHr { get; set; }
        public decimal? TotalUndertimeHr { get; set; }
        public decimal? TotalOvertimeHr { get; set; }
    }
}
