﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BlazorApp.Models.Responses
{
    public class HolidayDto
    {
        public int HolidayId { get; set; }
        [Required]
        public DateTime? StartDate { get; set; }
        [Required]
        public DateTime? EndDate { get; set; }
        [Required]
        public string HolidayType { get; set; }
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        public bool? Yearly { get; set; }

        [JsonIgnore]
        public string YearlyString { get; set; }

        public bool? Active { get; set; }

        [JsonIgnore]
        public string ActiveString { get; set; }

        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }
    }
}
