﻿namespace BlazorApp.Models.Responses
{
    /// <summary>
    /// The Api Result Dto.
    /// </summary>
    /// <seealso cref="BlazorApp.Models.Responses.ApiResponseDto" />
    public class ApiResultDto<T> : ApiResponseDto where T : new()
    {
        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        /// <value>
        /// The Data.
        /// </value>
        public T Data { get; set; }
    }
}
