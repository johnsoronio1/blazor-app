﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BlazorApp.Models.Responses
{
    public class WorkShiftDto
    {
        public int WorkShiftId { get; set; }
        [Required]
        [StringLength(60)]
        public string ShiftCode { get; set; }
        [Required]
        [StringLength(150)]
        public string ShiftDescription { get; set; }
        [Required]
        public TimeSpan TimeIn { get; set; }
        [Required]
        public TimeSpan TimeOut { get; set; }
        [Required]
        public TimeSpan? BreakStart { get; set; }
        [Required]
        public TimeSpan? BreakEnd { get; set; }
        public decimal? BreakHr { get; set; }
        public decimal? DailyWorkHr { get; set; }
        public decimal? LateHr { get; set; }
        public bool? AssumePresent { get; set; }
        [JsonIgnore]
        public string IsAssumePresentString { get; set; }
        public bool? Flexible { get; set; }
        [JsonIgnore]
        public string IsFlexibleString { get; set; }
        public bool? Monday { get; set; }
        [JsonIgnore]
        public string IsMondayString { get; set; }
        public bool? Tuesday { get; set; }
        [JsonIgnore]
        public string IsTuesdayString { get; set; }
        public bool? Wednesday { get; set; }
        [JsonIgnore]
        public string IsWednesdayString { get; set; }
        public bool? Thursday { get; set; }
        [JsonIgnore]
        public string IsThursdayString { get; set; }
        public bool? Friday { get; set; }
        [JsonIgnore]
        public string IsFridayString { get; set; }
        public bool? Saturday { get; set; }
        [JsonIgnore]
        public string IsSaturdayString { get; set; }
        public bool? Sunday { get; set; }
        [JsonIgnore]
        public string IsSundayString { get; set; }
        public bool? Active { get; set; }

        [JsonIgnore]
        public string IsActiveString { get; set; }
    }
}
