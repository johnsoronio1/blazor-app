﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp.Models.Requests
{
    public class UserEditModel
    {
        public int Id { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        public string RoleType { get; set; }

        public string LastLoggedIn { get; set; }
    }
}
