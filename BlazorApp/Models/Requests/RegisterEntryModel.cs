﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp.Models.Requests
{
    public class RegisterEntryModel
    {
        [Required(ErrorMessage = "* Username field is required.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "* Display Name field is required.")]
        public string DisplayName { get; set; }

        [Required(ErrorMessage = "* Password field is required.")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "* Confirm Password required")]
        [CompareAttribute("NewPassword", ErrorMessage = "* Password doesn't match.")]
        public string ConfirmPassword { get; set; }
    }
}

