﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Common
{
    public static class Global
    {
        public const string Bearer = "Bearer";
        public const string AccessToken = "AccessToken";
        public const string RefreshToken = "RefreshToken";
        public const string ApiEndpoint = "ApiEndpoint";
    }
}
