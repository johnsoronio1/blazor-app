﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Common
{
    public enum RolesEnum
    {
        Administrator,
        Accounting,
        Employee,
        Manager,
        User
    }
}
