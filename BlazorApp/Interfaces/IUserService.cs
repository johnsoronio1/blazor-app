﻿using System.Threading.Tasks;
using BlazorApp.Models.Requests;
using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IUserService
    {
        Task<ApiResponseDto> GetList();
        Task<ApiResponseDto> GetById(int id);
        Task<ApiResponseDto> Create(UserEntry user);
        Task<ApiResponseDto> Update(UserEditModel user);
        Task<ApiResponseDto> Delete(int id);
    }
}
