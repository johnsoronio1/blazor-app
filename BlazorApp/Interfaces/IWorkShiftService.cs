﻿using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IWorkShiftService : IService<WorkShiftDto>
    {

    }
}
