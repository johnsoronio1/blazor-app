﻿using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IEmployeeService : IService<EmployeeDto>
    {

    }
}
