﻿using System.Threading.Tasks;
using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IHttpClientService
    {
        Task<ApiResponseDto> GetJsonAsync(string path);
        Task<ApiResponseDto> PostJsonAsync(string path);
        Task<ApiResponseDto> PostJsonAsync<T>(string path, T formProperties) where T : new();
        Task<ApiResponseDto> PutJsonAsync(string path);
        Task<ApiResponseDto> PutJsonAsync<T>(string path, T formProperties) where T : new();
        Task<ApiResponseDto> DeleteJsonAsync(string path);
    }                                      
}
