﻿namespace BlazorApp.Interfaces
{
    public interface IUserSession
    {
        int UserId { get; }
        string Username { get; }
        string BearerToken { get; }
        string DisplayName { get; }
        bool IsAuthenticated { get; }
    }
}
