﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IService<T> where T : new()
    {
        Task<ApiResultDto<List<T>>> GetListAsync();
        Task<ApiResultDto<T>> GetByIdAsync(int id);
        Task<ApiResultDto<T>> CreateAsync(T model);
        Task<ApiResultDto<T>> UpdateAsync(T model);
        Task<bool> DeleteAsync(int id);
    }
}
