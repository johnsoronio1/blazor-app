﻿using BlazorApp.Models;

namespace BlazorApp.Interfaces
{
    public interface IAttendanceService : IService<AttendanceDto>
    {
    }
}
