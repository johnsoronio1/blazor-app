﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IEmployeeLogService : IService<LogDto>
    { 
        Task<ApiResultDto<List<LogDto>>> GetLogsByEmployee();
        Task<ApiResultDto<bool>> TimeIn();
        Task<ApiResultDto<bool>> TimeOUT();
    }
}
