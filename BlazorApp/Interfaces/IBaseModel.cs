﻿using System;

namespace BlazorApp.Interfaces
{
    public interface IBaseModel
    {
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }
    }
}
