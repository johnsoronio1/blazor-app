﻿using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IHolidayService : IService<HolidayDto>
    {

    }
}
