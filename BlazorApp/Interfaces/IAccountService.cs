﻿using System.Threading.Tasks;
using BlazorApp.Models;
using BlazorApp.Models.Requests;
using BlazorApp.Models.Responses;

namespace BlazorApp.Interfaces
{
    public interface IAccountService
    {
        Task<ApiResponseDto> LoginAsync(Credentials credentials);
        Task<ApiResponseDto> RegisterAsync(UserEntry entry);
        Task<ApiResponseDto> LogoutAsync(string refreshToken);
    }
}
