﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BlazorApp.Models;

namespace BlazorApp.Service.Interface
{
    public interface IService<TEntity, TModel>
               where TEntity : class, new()
               where TModel : class, new()
    {
        PaginatedList<TModel> PaginatedList(PagingDto model);
        Task<TModel> GetAsync(Expression<Func<TEntity, bool>> expression);
        Task<TModel> GetByIdAsync(int id);
        Task<List<TModel>> ListAsync();
        Task<List<TModel>> ListAsync(Expression<Func<TEntity, bool>> expression);
        Task<TModel> CreateAsync(TModel viewmodel);
        Task<TModel> UpdateAsync(int id, TModel viewmodel);
        Task<bool> DeleteAsync(int id);
        bool Any(Expression<Func<TEntity, bool>> expression);
    }
}
