﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace BlazorApp.Service.Interface
{
    public interface IAntiForgeryCookieService
    {
        void RegenerateAntiForgeryCookies(IEnumerable<Claim> claims);
        void DeleteAntiForgeryCookies();
    }
}
