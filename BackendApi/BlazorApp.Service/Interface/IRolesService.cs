﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorApp.Entities;

namespace BlazorApp.Service.Interface
{
    public interface IRolesService
    {
        Task<List<Role>> FindUserRolesAsync(int userId);
        Task<bool> IsUserInRoleAsync(int userId, string roleName);
        Task<List<User>> FindUsersInRoleAsync(string roleName);
    }
}
