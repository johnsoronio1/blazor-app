﻿using System.Threading.Tasks;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Service.Interface
{
    public interface IEmployeeLogService : IService<Logs, LogDto>
    {
        Task TimeIn(int employeeId);
        Task TimeOut(int employeeId);
    }
}
