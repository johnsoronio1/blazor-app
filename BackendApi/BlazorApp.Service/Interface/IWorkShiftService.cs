﻿using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Service.Interface
{
    public interface IWorkShiftService: IService<WorkShift, WorkShiftDto>
    {
    }
}
