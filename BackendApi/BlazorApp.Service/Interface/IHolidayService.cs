﻿using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Service.Interface
{
    public interface IHolidayService : IService<Holiday, HolidayDto>
    {

    }
}
