﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace BlazorApp.Service.Interface
{
    public interface ITokenValidatorService
    {
        Task ValidateAsync(TokenValidatedContext context);
    }
}
