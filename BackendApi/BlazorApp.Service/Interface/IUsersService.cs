﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Service.Interface
{
    public interface IUsersService
    {
        Task<string> GetSerialNumberAsync(int userId);
        Task<User> FindUserAsync(string username, string password);
        Task<User> FindUserByUsernameAsync(string username);
        Task<User> FindUserAsync(int userId);
        Task UpdateUserLastActivityDateAsync(int userId);
        Task<User> GetCurrentUserAsync();
        int GetCurrentUserId();
        Task<(bool Succeeded, string Error)> ChangePasswordAsync(User user, string currentPassword, string newPassword);
        Task<List<UserDto>> GetAll();
        Task<UserDto> GetbyId(int id);
        Task<UserDto> CreateAsync(UserEntry user);
        Task<UserDto> UpdateAsync(int id, UserDto model);
        Task<bool> DeleteAsync(int id);
    }
}
