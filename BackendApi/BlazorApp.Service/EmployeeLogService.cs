﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Repository;
using BlazorApp.Repository.Interface;
using BlazorApp.Service.Interface;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Service
{
    public class EmployeeLogService : IEmployeeLogService
    {
        private readonly DbSet<Logs> _logs;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;
        private readonly Repository<Logs> _repository;

        public EmployeeLogService(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            //_uow.CheckArgumentIsNull(nameof(_uow));

            _logs = _uow.Set<Logs>();

            _mapper = mapper;
            _repository = new Repository<Logs>(uow);
        }

        public bool Any(Expression<Func<Logs, bool>> expression)
        {
            return _repository.Where(expression).Any();
        }

        public async Task<LogDto> CreateAsync(LogDto viewmodel)
        {
            var entity = _mapper.Map<Logs>(viewmodel);
            var outputModel = new LogDto();

            entity = await _repository.CreateAsync(entity);
            outputModel = _mapper.Map<LogDto>(entity);

            return outputModel;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _repository.FirstOrDefaultAsync(m => m.LogId == id);
            if (entity != null)
            {
                try
                {
                    await _repository.DeleteAsync(entity);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public async Task<LogDto> GetAsync(Expression<Func<Logs, bool>> expression)
        {
            var entity = await _repository.FirstOrDefaultAsync(expression);
            if (entity != null)
            {
                return _mapper.Map<LogDto>(entity); ;
            }

            return new LogDto();
        }

        public async Task<LogDto> GetByIdAsync(int id)
        {
            var entity = await _repository.FirstOrDefaultAsync(m => m.LogId == id);
            if (entity != null)
            {
                return _mapper.Map<LogDto>(entity); ;
            }

            return new LogDto();
        }

        public async Task<List<LogDto>> ListAsync()
        {
            var viewmodels = await _logs
                .Include(m => m.Employee)
                .Select(m => new LogDto()
                {
                    LogId = m.LogId,
                    EmployeeId = m.EmployeeId,
                    EmployeeFirstName = m.Employee != null ? m.Employee.FirstName : "",
                    EmployeeLastName = m.Employee != null ? m.Employee.LastName : "",
                    TimeIn = m.TimeIn,
                    TimeOut = m.TimeOut,
                    DateCreated = m.DateCreated,
                    DateCreatedBy = m.DateCreatedBy,
                    DateUpdated = m.DateUpdated,
                    DateUpdatedBy = m.DateUpdatedBy
                })
                .OrderByDescending(m => m.DateCreated)
                .ToListAsync();

            return viewmodels;
        }

        public async Task<List<LogDto>> ListAsync(Expression<Func<Logs, bool>> expression)
        {
            var viewmodels = await _logs
                .Include(m => m.Employee)
                .Where(expression)
                .Select(m => new LogDto() { 
                    LogId = m.LogId,
                    EmployeeId = m.EmployeeId,
                    EmployeeFirstName = m.Employee != null ? m.Employee.FirstName : "",
                    EmployeeLastName = m.Employee != null ? m.Employee.LastName : "",
                    TimeIn = m.TimeIn,
                    TimeOut = m.TimeOut,
                    DateCreated = m.DateCreated,
                    DateCreatedBy = m.DateCreatedBy,
                    DateUpdated = m.DateUpdated,
                    DateUpdatedBy = m.DateUpdatedBy
                })
                .OrderByDescending(m => m.DateCreated)
                .ToListAsync();

            return viewmodels;
        }

        public PaginatedList<LogDto> PaginatedList(PagingDto model)
        {
            throw new NotImplementedException();
        }

        public async Task TimeIn(int employeeId)
        {
            var entity = new Logs();
            entity.TimeIn = DateTime.Now;
            entity.EmployeeId = employeeId;

            await _repository.CreateAsync(entity);
        }

        public async Task TimeOut(int employeeId)
        {
            var entity = new Logs();
            entity.TimeOut = DateTime.Now;
            entity.EmployeeId = employeeId;

            await _repository.CreateAsync(entity);
        }

        public Task<LogDto> UpdateAsync(int id, LogDto updateDto)
        {
            throw new NotImplementedException();
        }
    }
}
