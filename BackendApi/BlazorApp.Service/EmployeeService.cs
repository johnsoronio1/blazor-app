﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Repository;
using BlazorApp.Repository.Interface;
using BlazorApp.Service.Interface;

namespace BlazorApp.Service
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IMapper _mapper;
        private readonly Repository<Employee> _repository;

        public EmployeeService(IUnitOfWork uow, IMapper mapper) 
        {
            _mapper = mapper;
            _repository = new Repository<Employee>(uow);
        }

        public bool Any(Expression<Func<Employee, bool>> expression)
        {
            return _repository.Where(expression).Any();
        }

        public async Task<EmployeeDto> CreateAsync(EmployeeDto viewmodel)
        {
            var employeeEntity = _mapper.Map<Employee>(viewmodel);
            var outputModel = new EmployeeDto();

            employeeEntity = await _repository.CreateAsync(employeeEntity);
            outputModel = _mapper.Map<EmployeeDto>(employeeEntity);

            return outputModel;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var employeeEntity = await _repository.FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (employeeEntity != null)
            {
                try
                {
                    await _repository.DeleteAsync(employeeEntity);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public async Task<EmployeeDto> GetAsync(Expression<Func<Employee, bool>> expression)
        {
            var employeeEntity = await _repository.FirstOrDefaultAsync(expression);
            if (employeeEntity != null)
            {
                return _mapper.Map<EmployeeDto>(employeeEntity);
            }

            return new EmployeeDto();
        }

        public async Task<EmployeeDto> GetByIdAsync(int id)
        {
            var employeeEntity = await _repository.FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (employeeEntity != null)
            {
                return _mapper.Map<EmployeeDto>(employeeEntity); ;
            }

            return new EmployeeDto();
        }

        public async Task<List<EmployeeDto>> ListAsync()
        {
            var employees = await _repository.GetListAsync();
            if (employees != null && employees.Any())
            {
                return _mapper.Map<List<EmployeeDto>>(employees);  
            }

            return new List<EmployeeDto>();
        }

        public async Task<List<EmployeeDto>> ListAsync(Expression<Func<Employee, bool>> expression)
        {
            var employees = await _repository.GetListAsync(expression);
            if (employees != null && employees.Any())
            {
                return _mapper.Map<List<EmployeeDto>>(employees);
            }

            return new List<EmployeeDto>();
        }

        public async Task<EmployeeDto> UpdateAsync(int id, EmployeeDto viewmodel)
        {
            var employeeEntity = new Employee();
            employeeEntity = await _repository.FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (employeeEntity != null)
            {
                employeeEntity = _mapper.Map(viewmodel, employeeEntity);
                employeeEntity = await _repository.UpdateAsync(employeeEntity);

                return _mapper.Map<EmployeeDto>(employeeEntity);
            }
            return new EmployeeDto();
        }

        public PaginatedList<EmployeeDto> PaginatedList(PagingDto model)
        {
            throw new NotImplementedException();
        }
    }
}
