﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Repository;
using BlazorApp.Repository.Interface;
using BlazorApp.Service.Interface;

namespace BlazorApp.Service
{
    public class HolidayService : IHolidayService
    {
        private readonly IMapper _mapper;
        private readonly Repository<Holiday> _repository;

        public HolidayService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            _repository = new Repository<Holiday>(uow);
        }

        public bool Any(Expression<Func<Holiday, bool>> expression)
        {
            return _repository.Where(expression).Any();
        }

        public async Task<HolidayDto> CreateAsync(HolidayDto viewmodel)
        {
            var HolidayEntity = _mapper.Map<Holiday>(viewmodel);
            var outputModel = new HolidayDto();

            HolidayEntity = await _repository.CreateAsync(HolidayEntity);
            outputModel = _mapper.Map<HolidayDto>(HolidayEntity);

            return outputModel;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var HolidayEntity = await _repository.FirstOrDefaultAsync(m => m.HolidayId == id);
            if (HolidayEntity != null)
            {
                try
                {
                    await _repository.DeleteAsync(HolidayEntity);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public async Task<HolidayDto> GetAsync(Expression<Func<Holiday, bool>> expression)
        {
            var HolidayEntity = await _repository.FirstOrDefaultAsync(expression);
            if (HolidayEntity != null)
            {
                return _mapper.Map<HolidayDto>(HolidayEntity);
            }

            return new HolidayDto();
        }

        public async Task<HolidayDto> GetByIdAsync(int id)
        {
            var HolidayEntity = await _repository.FirstOrDefaultAsync(m => m.HolidayId == id);
            if (HolidayEntity != null)
            {
                return _mapper.Map<HolidayDto>(HolidayEntity); ;
            }

            return new HolidayDto();
        }

        public async Task<List<HolidayDto>> ListAsync()
        {
            var Holidays = await _repository.GetListAsync();
            if (Holidays != null && Holidays.Any())
            {
                return _mapper.Map<List<HolidayDto>>(Holidays);
            }

            return new List<HolidayDto>();
        }

        public async Task<List<HolidayDto>> ListAsync(Expression<Func<Holiday, bool>> expression)
        {
            var Holidays = await _repository.GetListAsync(expression);
            if (Holidays != null && Holidays.Any())
            {
                return _mapper.Map<List<HolidayDto>>(Holidays);
            }

            return new List<HolidayDto>();
        }

        public async Task<HolidayDto> UpdateAsync(int id, HolidayDto updateDto)
        {
            var HolidayEntity = new Holiday();
            HolidayEntity = await _repository.FirstOrDefaultAsync(m => m.HolidayId == id);
            if (HolidayEntity != null)
            {
                HolidayEntity = _mapper.Map(updateDto, HolidayEntity);
                HolidayEntity = await _repository.UpdateAsync(HolidayEntity);

                return _mapper.Map<HolidayDto>(HolidayEntity);
            }
            return new HolidayDto();
        }

        public PaginatedList<HolidayDto> PaginatedList(PagingDto model)
        {
            throw new NotImplementedException();
        }
    }
}
