﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Exceptions;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Repository.Interface;
using BlazorApp.Service.Interface;

namespace BlazorApp.Service
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Attendance> _repository;

        public AttendanceService(IMapper mapper, IRepository<Attendance> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public bool Any(Expression<Func<Attendance, bool>> expression)
        {
            return _repository.Where(expression).Any();
        }

        public async Task<AttendanceDto> CreateAsync(AttendanceDto viewmodel)
        {
            var AttendanceEntity = _mapper.Map<Attendance>(viewmodel);
            var outputModel = new AttendanceDto();

            AttendanceEntity = await _repository.CreateAsync(AttendanceEntity);
            outputModel = _mapper.Map<AttendanceDto>(AttendanceEntity);

            return outputModel;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var AttendanceEntity = await _repository.FirstOrDefaultAsync(m => m.AttendanceId == id);
            if (AttendanceEntity != null)
            {
                try
                {
                    await _repository.DeleteAsync(AttendanceEntity);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public async Task<AttendanceDto> GetAsync(Expression<Func<Attendance, bool>> expression)
        {
            var AttendanceEntity = await _repository.FirstOrDefaultAsync(expression);
            if (AttendanceEntity != null)
            {
                return _mapper.Map<AttendanceDto>(AttendanceEntity);
            }

            return new AttendanceDto();
        }

        public async Task<AttendanceDto> GetByIdAsync(int id)
        {
            var apiError = new ApiError();
            var attendanceModel = await _repository.FirstOrDefaultAsync(m => m.AttendanceId == id);
            if (attendanceModel == null)
            {
                apiError.ErrorMessage = "";
                apiError.ErrorCode = 4040;

                throw new NotFoundException(apiError);
            }

            return _mapper.Map<AttendanceDto>(attendanceModel);
        }

        public async Task<List<AttendanceDto>> ListAsync()
        {
            var Attendances = await _repository.GetListAsync();
            if (Attendances != null && Attendances.Any())
            {
                return _mapper.Map<List<AttendanceDto>>(Attendances);
            }

            return new List<AttendanceDto>();
        }

        public async Task<List<AttendanceDto>> ListAsync(Expression<Func<Attendance, bool>> expression)
        {
            var Attendances = await _repository.GetListAsync(expression);
            if (Attendances != null && Attendances.Any())
            {
                return _mapper.Map<List<AttendanceDto>>(Attendances);
            }

            return new List<AttendanceDto>();
        }

        public async Task<AttendanceDto> UpdateAsync(int id, AttendanceDto updateDto)
        {
            var attendanceEntity = new Attendance();
            attendanceEntity = await _repository.FirstOrDefaultAsync(m => m.AttendanceId == id);
            if (attendanceEntity != null)
            {
                attendanceEntity = _mapper.Map(updateDto, attendanceEntity);
                attendanceEntity = await _repository.UpdateAsync(attendanceEntity);

                return _mapper.Map<AttendanceDto>(attendanceEntity);
            }
            return new AttendanceDto();
        }

        public PaginatedList<AttendanceDto> PaginatedList(PagingDto model)
        {
            throw new NotImplementedException();
        }
    }
}
