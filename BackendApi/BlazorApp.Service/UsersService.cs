﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Repository.Interface;
using BlazorApp.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Service
{
    public class UsersService : IUsersService
    {
        private readonly DbSet<User> _users;
        private readonly DbSet<Role> _roles;
        private readonly DbSet<UserRole> _userRoles;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;
        private readonly ISecurityService _securityService;
        private readonly IHttpContextAccessor _contextAccessor;

        public UsersService(
            IMapper mapper,
            IUnitOfWork uow,
            ISecurityService securityService,
            IHttpContextAccessor contextAccessor) 
        {
            _mapper = mapper;
            _uow = uow;
            _users = _uow.Set<User>();
            _roles = _uow.Set<Role>();
            _userRoles = _uow.Set<UserRole>();
            _securityService = securityService;
            _contextAccessor = contextAccessor;
        }

        public async Task<User> FindUserAsync(int userId)
        {
            return await _users.FindAsync(userId);
        }

        public Task<User> FindUserAsync(string username, string password)
        {
            var passwordHash = _securityService.GetSha256Hash(password);
            return _users.FirstOrDefaultAsync(x => x.Username == username && x.Password == passwordHash);
        }

        public async Task<string> GetSerialNumberAsync(int userId)
        {
            var user = await FindUserAsync(userId);
            return user.SerialNumber;
        }

        public async Task UpdateUserLastActivityDateAsync(int userId)
        {
            var user = await FindUserAsync(userId);
            if (user.LastLoggedIn != null)
            {
                var updateLastActivityDate = TimeSpan.FromMinutes(2);
                var currentUtc = DateTimeOffset.UtcNow;
                var timeElapsed = currentUtc.Subtract(user.LastLoggedIn.Value);
                if (timeElapsed < updateLastActivityDate)
                {
                    return;
                }
            }
            user.LastLoggedIn = DateTimeOffset.UtcNow;
            await _uow.SaveChangesAsync();
        }

        public int GetCurrentUserId()
        {
            var claimsIdentity = _contextAccessor.HttpContext.User.Identity as ClaimsIdentity;
            var userDataClaim = claimsIdentity?.FindFirst(ClaimTypes.UserData);
            var userId = userDataClaim?.Value;
            return string.IsNullOrWhiteSpace(userId) ? 0 : int.Parse(userId);
        }

        public Task<User> GetCurrentUserAsync()
        {
            var userId = GetCurrentUserId();
            return FindUserAsync(userId);
        }

        public async Task<(bool Succeeded, string Error)> ChangePasswordAsync(User user, string currentPassword, string newPassword)
        {
            var currentPasswordHash = _securityService.GetSha256Hash(currentPassword);
            if (user.Password != currentPasswordHash)
            {
                return (false, "Current password is wrong.");
            }

            user.Password = _securityService.GetSha256Hash(newPassword);
            // user.SerialNumber = Guid.NewGuid().ToString("N"); // To force other logins to expire.
            await _uow.SaveChangesAsync();
            return (true, string.Empty);
        }

        public async Task<List<UserDto>> GetAll()
        {
            return await _users.Select(user => new UserDto()
            {
                Id = user.Id,
                Username = user.Username,
                DisplayName = user.DisplayName,
                LastLoggedIn = user.LastLoggedIn != null ? user.LastLoggedIn.Value.UtcDateTime.ToString() : string.Empty
            }).ToListAsync();
        }

        public async Task<UserDto> GetbyId(int id)
        {
            return await _users.Select(user => new UserDto()
            {
                Id = user.Id,
                Username = user.Username,
                DisplayName = user.DisplayName,
                LastLoggedIn = user.LastLoggedIn != null ? user.LastLoggedIn.Value.UtcDateTime.ToString() : string.Empty
            }).Where(m => m.Id == id).FirstOrDefaultAsync();
        }

        public async Task<UserDto> CreateAsync(UserEntry user)
        {
            var userRole = _roles.Where(m => m.Name == CustomRoles.User).FirstOrDefault();
            var userEntry = new User
            {
                Username = user.Username,
                DisplayName = user.DisplayName,
                IsActive = true,
                LastLoggedIn = null,
                Password = _securityService.GetSha256Hash(user.Password),
                SerialNumber = Guid.NewGuid().ToString("N")
            };

            _users.Add(userEntry);
            _userRoles.Add(new UserRole { Role = userRole, User = userEntry });

            await _uow.SaveChangesAsync();

            var userModel = new UserDto();
            userModel.Id = userEntry.Id;
            userModel.Username = userEntry.Username;
            userModel.DisplayName = userEntry.DisplayName;

            return userModel;
        }

        public async Task<UserDto> UpdateAsync(int id, UserDto model)
        {
            var userEntity = _users.Where(m => m.Id == id).FirstOrDefault();
            if (userEntity != null)
            {
                userEntity.DisplayName = model.DisplayName;

                _users.Update(userEntity);

                await _uow.SaveChangesAsync();
            }
            return _mapper.Map<UserDto>(userEntity);
        }

        public async Task<bool> DeleteAsync(int id)
        { 
            var userEntity = _users.Where(m => m.Id == id).FirstOrDefault();
            if (userEntity != null)
            {
                try
                {
                    _users.Remove(userEntity);

                    await _uow.SaveChangesAsync();

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public async Task<User> FindUserByUsernameAsync(string username)
        {
            return await _users.Where(m => m.Username == username).FirstOrDefaultAsync();
        }
    }
}
