﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Repository;
using BlazorApp.Repository.Interface;
using BlazorApp.Service.Interface;

namespace BlazorApp.Service
{
    public class WorkShiftService : IWorkShiftService
    {
        private readonly IMapper _mapper;
        private readonly Repository<WorkShift> _repository;

        public WorkShiftService(IUnitOfWork uow, IMapper mapper)
        {
            _mapper = mapper;
            _repository = new Repository<WorkShift>(uow);
        }

        public bool Any(Expression<Func<WorkShift, bool>> expression)
        {
            return _repository.Where(expression).Any();
        }

        public async Task<WorkShiftDto> CreateAsync(WorkShiftDto viewmodel)
        {
            var WorkShiftEntity = _mapper.Map<WorkShift>(viewmodel);
            var outputModel = new WorkShiftDto();

            WorkShiftEntity = await _repository.CreateAsync(WorkShiftEntity);
            outputModel = _mapper.Map<WorkShiftDto>(WorkShiftEntity);

            return outputModel;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var WorkShiftEntity = await _repository.FirstOrDefaultAsync(m => m.WorkShiftId == id);
            if (WorkShiftEntity != null)
            {
                try
                {
                    await _repository.DeleteAsync(WorkShiftEntity);

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public async Task<WorkShiftDto> GetAsync(Expression<Func<WorkShift, bool>> expression)
        {
            var WorkShiftEntity = await _repository.FirstOrDefaultAsync(expression);
            if (WorkShiftEntity != null)
            {
                return _mapper.Map<WorkShiftDto>(WorkShiftEntity);
            }

            return new WorkShiftDto();
        }

        public async Task<WorkShiftDto> GetByIdAsync(int id)
        {
            var WorkShiftEntity = await _repository.FirstOrDefaultAsync(m => m.WorkShiftId == id);
            if (WorkShiftEntity != null)
            {
                return _mapper.Map<WorkShiftDto>(WorkShiftEntity); ;
            }

            return new WorkShiftDto();
        }

        public async Task<List<WorkShiftDto>> ListAsync()
        {
            var WorkShifts = await _repository.GetListAsync();
            if (WorkShifts != null && WorkShifts.Any())
            {
                return _mapper.Map<List<WorkShiftDto>>(WorkShifts);
            }

            return new List<WorkShiftDto>();
        }

        public async Task<List<WorkShiftDto>> ListAsync(Expression<Func<WorkShift, bool>> expression)
        {
            var WorkShifts = await _repository.GetListAsync(expression);
            if (WorkShifts != null && WorkShifts.Any())
            {
                return _mapper.Map<List<WorkShiftDto>>(WorkShifts);
            }

            return new List<WorkShiftDto>();
        }

        public async Task<WorkShiftDto> UpdateAsync(int id, WorkShiftDto viewmodel)
        {
            var WorkShiftEntity = new WorkShift();
            WorkShiftEntity = await _repository.FirstOrDefaultAsync(m => m.WorkShiftId == id);
            if (WorkShiftEntity != null)
            {
                WorkShiftEntity = _mapper.Map(viewmodel, WorkShiftEntity);
                WorkShiftEntity = await _repository.UpdateAsync(WorkShiftEntity);

                return _mapper.Map<WorkShiftDto>(WorkShiftEntity);
            }
            return new WorkShiftDto();
        }

        public PaginatedList<WorkShiftDto> PaginatedList(PagingDto model)
        {
            throw new NotImplementedException();
        }
    }
}
