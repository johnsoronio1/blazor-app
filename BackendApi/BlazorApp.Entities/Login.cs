﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class Login : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LoginId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(60)]
        public string Username { get; set; }
        [Required]
        [StringLength(150)]
        public string Password { get; set; }
        [Required]
        [StringLength(100)]
        public string EmailAddress { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateLastLogin { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
