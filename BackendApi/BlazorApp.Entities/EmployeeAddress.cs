﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class EmployeeAddress : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpAddressId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(150)]
        public string AddressType { get; set; }
        [StringLength(150)]
        public string AddressLine1 { get; set; }
        [StringLength(150)]
        public string AddressLine2 { get; set; }
        [StringLength(150)]
        public string City { get; set; }
        [StringLength(100)]
        public string Country { get; set; }
        public int? PostalCode { get; set; }
        public bool? IsCurrentAddress { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
