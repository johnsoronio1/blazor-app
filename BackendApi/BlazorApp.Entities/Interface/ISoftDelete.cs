﻿namespace BlazorApp.Entities.Interface
{
    public interface ISoftDelete
    {
        public bool? IsDeleted { get; set; }
    }
}
