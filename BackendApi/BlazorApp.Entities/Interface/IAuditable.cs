﻿using System;

namespace BlazorApp.Entities.Interface
{
    public interface IAuditable
    {
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }
    }
}
