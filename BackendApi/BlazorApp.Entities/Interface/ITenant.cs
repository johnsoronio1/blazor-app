﻿namespace BlazorApp.Entities.Interface
{
    public interface ITenant
    {
        public int? TenantId { get; set; }
    }
}
