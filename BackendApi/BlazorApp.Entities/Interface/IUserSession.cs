﻿using System.Collections.Generic;

namespace BlazorApp.Entities.Interface
{
    public interface IUserSession
    {
        int UserId { get; set; }
        int TenantId { get; set; }
        List<string> Roles { get; set; }
        string Username { get; set; }
        bool DisableTenantFilter { get; set; }
    }
}
