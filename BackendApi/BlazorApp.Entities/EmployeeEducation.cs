﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class EmployeeEducation : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpEducationId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(30)]
        public string EducationType { get; set; }
        [Required]
        [StringLength(30)]
        public string EducationLevel { get; set; }
        [Required]
        [StringLength(100)]
        public string SchoolName { get; set; }
        [StringLength(150)]
        public string AddressLine1 { get; set; }
        [StringLength(150)]
        public string AddressLine2 { get; set; }
        public DateTime? YearStarted { get; set; }
        public DateTime? YearCompleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
