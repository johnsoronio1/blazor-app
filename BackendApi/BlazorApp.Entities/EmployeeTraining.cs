﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class EmployeeTraining : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpTraining { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(150)]
        public string TrainingName { get; set; }
        [StringLength(250)]
        public string TrainingDescription { get; set; }
        [Required]
        public DateTime DateTaken { get; set; }
        [StringLength(150)]
        public string Venue { get; set; }
        [StringLength(250)]
        public string Remarks { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
