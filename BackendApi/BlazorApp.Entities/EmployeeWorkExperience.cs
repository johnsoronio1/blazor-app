﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class EmployeeWorkExperience : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpExperience { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [StringLength(60)]
        public string Position { get; set; }
        public decimal? BasicPay { get; set; }
        public decimal? Allowance { get; set; }
        [StringLength(150)]
        public string Company { get; set; }
        [StringLength(150)]
        public string AddressLine1 { get; set; }
        [StringLength(150)]
        public string AddressLine2 { get; set; }
        public DateTime? DateStarted { get; set; }
        public DateTime? DateEnded { get; set; }
        [StringLength(150)]
        public string ReasonLeave { get; set; }
        [StringLength(150)]
        public string OtherReason { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
