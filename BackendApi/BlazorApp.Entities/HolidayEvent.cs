﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class HolidayEvent : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HolidayEventId { get; set; }
        [Required]
        public int HolidayId { get; set; }
        [Required]
        public int GroupId { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual GroupManager Group { get; set; }
        public virtual Holiday Holiday { get; set; }
    }
}
