﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class LibraryManager : IAuditable
    {
        public LibraryManager()
        {
            LibraryItem = new HashSet<LibraryItem>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LibraryId { get; set; }
        [Required]
        [StringLength(60)]
        public string LibraryName { get; set; }
        [StringLength(150)]
        public string LibraryDescription { get; set; }
        public int? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual ICollection<LibraryItem> LibraryItem { get; set; }
    }
}
