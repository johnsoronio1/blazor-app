﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class EmployeeDocument : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpDocument { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(30)]
        public string DocumentType { get; set; }
        [Required]
        [StringLength(150)]
        public string DocumentName { get; set; }
        public DateTime? DocumentUploaded { get; set; }
        [StringLength(250)]
        public string Remarks { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
