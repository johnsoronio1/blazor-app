﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class WorkShift : IAuditable
    {
        public WorkShift()
        {
            EmployeeShift = new HashSet<EmployeeShift>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkShiftId { get; set; }
        [Required]
        [StringLength(60)]
        public string Code { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        [Required]
        public DateTime TimeIn { get; set; }
        [Required]
        public DateTime TimeOut { get; set; }
        [Required]
        public DateTime? BreakStart { get; set; }
        [Required]
        public DateTime? BreakEnd { get; set; }
        public decimal? BreakHr { get; set; }
        public decimal? DailyWorkHr { get; set; }
        public decimal? LateHr { get; set; }
        public bool? AssumePresent { get; set; }
        public bool? Flexible { get; set; }
        public bool? Monday { get; set; }
        public bool? Tuesday { get; set; }
        public bool? Wednesday { get; set; }
        public bool? Thursday { get; set; }
        public bool? Friday { get; set; }
        public bool? Saturday { get; set; }
        public bool? Sunday { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual ICollection<EmployeeShift> EmployeeShift { get; set; }
    }
}
