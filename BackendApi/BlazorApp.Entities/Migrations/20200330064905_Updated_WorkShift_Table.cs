﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BlazorApp.Entities.Migrations
{
    public partial class Updated_WorkShift_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkShiftSched");

            migrationBuilder.DropColumn(
                name: "ShiftCode",
                table: "WorkShift");

            migrationBuilder.DropColumn(
                name: "ShiftDescription",
                table: "WorkShift");

            migrationBuilder.AlterColumn<DateTime>(
                name: "TimeOut",
                table: "WorkShift",
                nullable: false,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AlterColumn<DateTime>(
                name: "TimeIn",
                table: "WorkShift",
                nullable: false,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AlterColumn<DateTime>(
                name: "BreakStart",
                table: "WorkShift",
                nullable: false,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AlterColumn<DateTime>(
                name: "BreakEnd",
                table: "WorkShift",
                nullable: false,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "WorkShift",
                maxLength: 60,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "WorkShift",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "EffectiveFrom",
                table: "WorkShift",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EffectiveTo",
                table: "WorkShift",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "WorkShift");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "WorkShift");

            migrationBuilder.DropColumn(
                name: "EffectiveFrom",
                table: "WorkShift");

            migrationBuilder.DropColumn(
                name: "EffectiveTo",
                table: "WorkShift");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "TimeOut",
                table: "WorkShift",
                type: "time",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "TimeIn",
                table: "WorkShift",
                type: "time",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "BreakStart",
                table: "WorkShift",
                type: "time",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "BreakEnd",
                table: "WorkShift",
                type: "time",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "ShiftCode",
                table: "WorkShift",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ShiftDescription",
                table: "WorkShift",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "WorkShiftSched",
                columns: table => new
                {
                    SchedShiftId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Active = table.Column<bool>(type: "bit", nullable: true),
                    BreakEnd = table.Column<TimeSpan>(type: "time", nullable: false),
                    BreakHr = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    BreakStart = table.Column<TimeSpan>(type: "time", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateCreatedBy = table.Column<int>(type: "int", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateUpdatedBy = table.Column<int>(type: "int", nullable: true),
                    LateHr = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ScheduleDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TimeIn = table.Column<TimeSpan>(type: "time", nullable: false),
                    TimeOut = table.Column<TimeSpan>(type: "time", nullable: false),
                    WorkHr = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    WorkShiftId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkShiftSched", x => x.SchedShiftId);
                    table.ForeignKey(
                        name: "FK_WorkShiftSched_WorkShift_WorkShiftId",
                        column: x => x.WorkShiftId,
                        principalTable: "WorkShift",
                        principalColumn: "WorkShiftId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkShiftSched_WorkShiftId",
                table: "WorkShiftSched",
                column: "WorkShiftId");
        }
    }
}
