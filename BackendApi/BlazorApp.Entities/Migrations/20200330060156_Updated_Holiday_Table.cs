﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BlazorApp.Entities.Migrations
{
    public partial class Updated_Holiday_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EffectiveDate",
                table: "Holiday");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "Holiday",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Holiday",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "Holiday");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Holiday");

            migrationBuilder.AddColumn<DateTime>(
                name: "EffectiveDate",
                table: "Holiday",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
