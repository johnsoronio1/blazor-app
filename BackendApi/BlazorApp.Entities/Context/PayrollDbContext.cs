﻿using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Entities.Context
{
    public partial class PayrollDbContext : DbContext
    {
        public virtual DbSet<User> Users { set; get; }
        public virtual DbSet<Role> Roles { set; get; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserToken> UserTokens { get; set; }
        public virtual DbSet<Attendance> Attendance { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EmployeeAddress> EmployeeAddress { get; set; }
        public virtual DbSet<EmployeeBank> EmployeeBank { get; set; }
        public virtual DbSet<EmployeeContact> EmployeeContact { get; set; }
        public virtual DbSet<EmployeeDocument> EmployeeDocument { get; set; }
        public virtual DbSet<EmployeeEducation> EmployeeEducation { get; set; }
        public virtual DbSet<EmployeeLeave> EmployeeLeave { get; set; }
        public virtual DbSet<EmployeeShift> EmployeeShift { get; set; }
        public virtual DbSet<EmployeeSkill> EmployeeSkill { get; set; }
        public virtual DbSet<EmployeeTraining> EmployeeTraining { get; set; }
        public virtual DbSet<EmployeeWorkExperience> EmployeeWorkExperience { get; set; }
        public virtual DbSet<GroupManager> GroupManager { get; set; }
        public virtual DbSet<GroupMember> GroupMember { get; set; }
        public virtual DbSet<Holiday> Holiday { get; set; }
        public virtual DbSet<HolidayEvent> HolidayEvent { get; set; }
        public virtual DbSet<Leave> Leave { get; set; }
        public virtual DbSet<LibraryItem> LibraryItem { get; set; }
        public virtual DbSet<LibraryManager> LibraryManager { get; set; }
        public virtual DbSet<Login> Login { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }
        public virtual DbSet<WorkShift> WorkShift { get; set; }
        public virtual DbSet<ApiLogItem> ApiLogItems { get; set; }
        public virtual DbSet<Tenant> Tenants { get; set; }

        public PayrollDbContext(DbContextOptions options) : base(options) 
        { 
        
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
