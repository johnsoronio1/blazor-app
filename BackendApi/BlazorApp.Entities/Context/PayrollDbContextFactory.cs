﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace BlazorApp.Entities.Context
{
    /// <summary>
    /// Only used by EF Tooling
    /// </summary>
    public class PayrollDbContextFactory : IDesignTimeDbContextFactory<PayrollDbContext>
    {
        public PayrollDbContext CreateDbContext(string[] args)
        {
            var basePath = Directory.GetCurrentDirectory();

            Console.WriteLine("EntityFramework Migrations: ");
            Console.WriteLine($"Using `{basePath}` as the BasePath");

            var configuration = new ConfigurationBuilder()
                                    .SetBasePath(basePath)
                                    .AddJsonFile("appsettings.json")
                                    .Build();

            var builder = new DbContextOptionsBuilder<PayrollDbContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection").Replace("|DataDirectory|", Path.Combine(basePath, "wwwroot", "app_data"));

            builder.UseSqlServer(connectionString);
            return new PayrollDbContext(builder.Options);
        }
    }
}
