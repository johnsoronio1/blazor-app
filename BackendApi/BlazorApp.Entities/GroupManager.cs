﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class GroupManager : IAuditable
    {
        public GroupManager()
        {
            GroupMember = new HashSet<GroupMember>();
            HolidayEvent = new HashSet<HolidayEvent>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupId { get; set; }
        [Required]
        [StringLength(30)]
        public string GroupType { get; set; }
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        [StringLength(150)]
        public string Description { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual ICollection<GroupMember> GroupMember { get; set; }
        public virtual ICollection<HolidayEvent> HolidayEvent { get; set; }
    }
}
