﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class Employee : IAuditable
    {
        public Employee()
        {
            Attendance = new HashSet<Attendance>();
            EmployeeAddress = new HashSet<EmployeeAddress>();
            EmployeeBank = new HashSet<EmployeeBank>();
            EmployeeContact = new HashSet<EmployeeContact>();
            EmployeeDocument = new HashSet<EmployeeDocument>();
            EmployeeEducation = new HashSet<EmployeeEducation>();
            EmployeeLeave = new HashSet<EmployeeLeave>();
            EmployeeShift = new HashSet<EmployeeShift>();
            EmployeeSkill = new HashSet<EmployeeSkill>();
            EmployeeTraining = new HashSet<EmployeeTraining>();
            EmployeeWorkExperience = new HashSet<EmployeeWorkExperience>();
            GroupMember = new HashSet<GroupMember>();
            Login = new HashSet<Login>();
            Logs = new HashSet<Logs>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeId { get; set; }
        public int? EmployeeCompanyId { get; set; }
        public int? UserId { get; set; }
        [Required]
        [StringLength(60)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(60)]
        public string MiddleName { get; set; }
        [Required]
        [StringLength(60)]
        public string LastName { get; set; }
        [StringLength(30)]
        public string PrefixName { get; set; }
        [StringLength(30)]
        public string SuffixName { get; set; }
        [Required]
        public int? Age { get; set; }
        [Required]
        [StringLength(10)]
        public string Gender { get; set; }
        public bool? Active { get; set; }
        [Required]
        public DateTime? BirthDate { get; set; }
        [Required]
        public DateTime? HiredDate { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual ICollection<Attendance> Attendance { get; set; }
        public virtual ICollection<EmployeeAddress> EmployeeAddress { get; set; }
        public virtual ICollection<EmployeeBank> EmployeeBank { get; set; }
        public virtual ICollection<EmployeeContact> EmployeeContact { get; set; }
        public virtual ICollection<EmployeeDocument> EmployeeDocument { get; set; }
        public virtual ICollection<EmployeeEducation> EmployeeEducation { get; set; }
        public virtual ICollection<EmployeeLeave> EmployeeLeave { get; set; }
        public virtual ICollection<EmployeeShift> EmployeeShift { get; set; }
        public virtual ICollection<EmployeeSkill> EmployeeSkill { get; set; }
        public virtual ICollection<EmployeeTraining> EmployeeTraining { get; set; }
        public virtual ICollection<EmployeeWorkExperience> EmployeeWorkExperience { get; set; }
        public virtual ICollection<GroupMember> GroupMember { get; set; }
        public virtual ICollection<Login> Login { get; set; }
        public virtual ICollection<Logs> Logs { get; set; }

        public virtual Company Company { get; set; }
        public virtual User User { get; set; }
    }
}
