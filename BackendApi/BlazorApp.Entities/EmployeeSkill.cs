﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class EmployeeSkill : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmpSkill { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(100)]
        public string Skill { get; set; }
        [StringLength(150)]
        public string SkillDescription { get; set; }
        [StringLength(30)]
        public string SkillLevel { get; set; }
        public int? YearExperience { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
