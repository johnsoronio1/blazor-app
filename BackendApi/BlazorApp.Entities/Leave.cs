﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class Leave : IAuditable
    {
        public Leave()
        {
            EmployeeLeave = new HashSet<EmployeeLeave>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LeaveId { get; set; }
        [Required]
        [StringLength(30)]
        public string LeaveType { get; set; }
        [Required]
        [StringLength(150)]
        public string LeaveName { get; set; }
        [StringLength(150)]
        public string LeaveDescription { get; set; }
        public decimal? Credits { get; set; }
        public bool? Yearly { get; set; }
        public DateTime? RefreshDate { get; set; }
        public decimal? MaxCashConvert { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual ICollection<EmployeeLeave> EmployeeLeave { get; set; }
    }
}
