﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class LibraryItem : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LibraryItemId { get; set; }
        [Required]
        public int LibManagerId { get; set; }
        [Required]
        [StringLength(60)]
        public string Name { get; set; }
        [StringLength(150)]
        public string Description { get; set; }
        [StringLength(60)]
        public string Code { get; set; }
        [StringLength(250)]
        public string Value { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual LibraryManager LibManager { get; set; }
    }
}
