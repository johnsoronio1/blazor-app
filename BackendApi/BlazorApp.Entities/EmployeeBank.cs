﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public partial class EmployeeBank : IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeBankId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(100)]
        public string BankName { get; set; }
        [Required]
        [StringLength(150)]
        public string BankDescription { get; set; }
        [Required]
        [StringLength(100)]
        public string AccountNumber { get; set; }
        [Required]
        [StringLength(100)]
        public string AccountType { get; set; }
        public bool? IsCurrent { get; set; }
        [StringLength(150)]
        public string AddressLine1 { get; set; }
        [StringLength(150)]
        public string AddressLine2 { get; set; }
        [StringLength(30)]
        public string Currency { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
