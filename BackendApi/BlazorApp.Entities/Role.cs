﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BlazorApp.Entities.Interface;

namespace BlazorApp.Entities
{
    public class Role : IAuditable
    {
        public Role()
        {
            UserRoles = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(60)]
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
