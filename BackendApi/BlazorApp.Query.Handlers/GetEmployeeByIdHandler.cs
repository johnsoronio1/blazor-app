﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Requests;
using BlazorApp.Repository.Interface;
using MediatR;

namespace BlazorApp.Query.Handlers
{
    public class GetEmployeeByIdHandler : IRequestHandler<GetEmployeeByIdRequest, EmployeeDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Employee> _employeeRepository;

        public GetEmployeeByIdHandler(
            IMapper mapper,
            IRepository<Employee> employeeRepository)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public async Task<EmployeeDto> Handle(GetEmployeeByIdRequest request, CancellationToken cancellationToken)
        {
            var employee = await _employeeRepository.FirstOrDefaultAsync(m => m.EmployeeId == request.IdFromRoute);
            var resultDto = _mapper.Map<Employee, EmployeeDto>(employee);

            return resultDto;
        }
    }
}
