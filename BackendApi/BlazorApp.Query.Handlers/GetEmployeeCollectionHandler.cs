﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Requests;
using BlazorApp.Repository.Extensions;
using BlazorApp.Repository.Interface;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Query.Handlers
{
    public class GetEmployeeCollectionHandler : IRequestHandler<GetEmployeeCollectionRequest, PaginatedList<EmployeeDto>>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Employee> _employeeRepository;

        public GetEmployeeCollectionHandler(
            IMapper mapper,
            IRepository<Employee> employeeRepository)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public async Task<PaginatedList<EmployeeDto>> Handle(GetEmployeeCollectionRequest request, CancellationToken cancellationToken)
        {
            var query = _employeeRepository.AsQueryable();

            if (!string.IsNullOrEmpty(request.SortField))
            {
                switch (request.SortField)
                {
                    case "firstName":
                        if (request.SortByDesc)
                            query = query.OrderByDescending(m => m.FirstName);
                        else
                            query = query.OrderBy(m => m.FirstName);
                        break;
                    case "lastName":
                        if (request.SortByDesc)
                            query = query.OrderByDescending(m => m.LastName);
                        else
                            query = query.OrderBy(m => m.LastName);
                        break;
                }
            }
            else
                query = query.OrderBy(m => m.DateCreated);

            if (request.Filters != null && request.Filters.Any())
            {
                foreach (var filter in request.Filters)
                {
                    if (!string.IsNullOrEmpty(filter.Value))
                    {
                        switch (filter.Field)
                        {
                            case "firstName":
                                query = query.Where(m => m.FirstName.ToLower().Contains(filter.Value.ToLower()));
                                break;
                            case "lastName":
                                query = query.Where(m => m.LastName.ToLower().Contains(filter.Value.ToLower()));
                                break;
                        }
                    }
                }
            }

            var projection = query.Select(m => m).AsNoTracking();
            var results = await projection.ToPaginatedList(request.PageNumber, request.PageSize);
            var resultDtos = _mapper.Map<PaginatedList<Employee>, PaginatedList<EmployeeDto>>(results);

            return resultDtos;
        }
    }
}
