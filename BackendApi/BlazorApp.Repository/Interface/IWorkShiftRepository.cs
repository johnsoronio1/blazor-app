﻿using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IWorkShiftRepository : IRepository<WorkShift>
    {

    }
}
