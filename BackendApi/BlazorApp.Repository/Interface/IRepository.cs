﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlazorApp.Repository.Interface
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression);
        TEntity GetSingleWithTracking(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> GetSingleWithTrackingAsync(Expression<Func<TEntity, bool>> expression);

        List<TEntity> GetList();
        Task<List<TEntity>> GetListAsync();
        Task<List<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> AsQueryable();
        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> expression);
        TEntity Create(TEntity entity);
        Task<TEntity> CreateAsync(TEntity entity);
        void Delete(TEntity entity);
        Task DeleteAsync(TEntity entity);
        TEntity Update(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task SaveChangesAsync();
    }
}
