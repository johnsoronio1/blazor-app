﻿using System.Threading.Tasks;
using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IUserRepository : IRepository<User>
    {

    }
}
