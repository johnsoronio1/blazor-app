﻿using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IRoleRepository : IRepository<Role>
    {

    }
}
