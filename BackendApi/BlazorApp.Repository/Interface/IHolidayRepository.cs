﻿using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IHolidayRepository : IRepository<Holiday>
    {

    }
}
