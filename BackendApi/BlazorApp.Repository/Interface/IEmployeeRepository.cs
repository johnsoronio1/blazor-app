﻿using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IEmployeeRepository : IRepository<Employee>
    {

    }
}
