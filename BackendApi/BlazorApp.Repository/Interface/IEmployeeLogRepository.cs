﻿using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IEmployeeLogRepository  : IRepository<Logs>
    {

    }
}
