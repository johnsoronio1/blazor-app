﻿using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {

    }
}
