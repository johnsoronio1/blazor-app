﻿using BlazorApp.Entities;

namespace BlazorApp.Repository.Interface
{
    public interface IAttendanceRepository : IRepository<Attendance>
    {

    }
}
