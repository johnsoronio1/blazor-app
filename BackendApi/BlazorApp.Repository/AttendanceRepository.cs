﻿using BlazorApp.Entities;
using BlazorApp.Repository.Interface;

namespace BlazorApp.Repository
{
    public class AttendanceRepository : Repository<Attendance>, IAttendanceRepository
    {
        public AttendanceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }
}