﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BlazorApp.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Repository
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        internal IUnitOfWork unitOfWork;
        internal DbSet<TEntity> dbSet;

        public Repository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.dbSet = this.unitOfWork.Set<TEntity>();
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> where)
        {
            IQueryable<TEntity> entities = AsQueryable();
            TEntity entity = entities.Where(where).AsNoTracking().FirstOrDefault();
            return entity;
        }

        public TEntity GetSingleWithTracking(Expression<Func<TEntity, bool>> where)
        {
            IQueryable<TEntity> entities = AsQueryable();
            TEntity entity = entities.Where(where).FirstOrDefault();
            return entity;
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return dbSet.AsQueryable();
        }

        public List<TEntity> GetList()
        {
            return dbSet.ToList();
        }

        public TEntity Create(TEntity entity)
        {
            dbSet.Add(entity);
            unitOfWork.SaveChanges();

            return entity;
        }

        public void Delete(TEntity entity)
        {
            dbSet.Remove(entity);
            unitOfWork.SaveChanges();
        }

        public TEntity Update(TEntity entity)
        {
            dbSet.Attach(entity).State = EntityState.Modified;
            unitOfWork.SaveChanges();
            return entity;
        }
        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> where)
        {
            return dbSet.Where(where);
        }

        public IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> queryable = AsQueryable();
            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include(includeProperty);
            }

            return queryable;
        }

        public async Task<List<TEntity>> GetListAsync()
        {
            return await dbSet.ToListAsync();
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            await dbSet.AddAsync(entity);
            await SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(TEntity entity)
        {
            dbSet.Remove(entity);
            await SaveChangesAsync();
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            dbSet.Update(entity);
            await SaveChangesAsync();
            return entity;
        }

        public async Task SaveChangesAsync()
        {
            await unitOfWork.SaveChangesAsync();
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> expression)
        {
            IQueryable<TEntity> entities = AsQueryable();
            TEntity entity = await entities.Where(expression).AsNoTracking().FirstOrDefaultAsync();
            return entity;
        }

        public async Task<TEntity> GetSingleWithTrackingAsync(Expression<Func<TEntity, bool>> expression)
        {
            IQueryable<TEntity> entities = AsQueryable();
            TEntity entity = await entities.Where(expression).FirstOrDefaultAsync();
            return entity;
        }

        public async Task<List<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await dbSet.Where(expression).ToListAsync();
        }

        public enum OrderByType
        {
            Ascending,
            Descending
        }
    }
}
