﻿using BlazorApp.Entities;
using BlazorApp.Repository.Interface;

namespace BlazorApp.Repository
{
    public class EmployeeLogRepository : Repository<Logs>, IEmployeeLogRepository
    {
        public EmployeeLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }
}