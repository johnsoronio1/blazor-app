﻿using BlazorApp.Entities;
using BlazorApp.Repository.Interface;

namespace BlazorApp.Repository
{
    public class WorkShiftRepository : Repository<WorkShift>, IWorkShiftRepository
    {
        public WorkShiftRepository(IUnitOfWork unitOfWork) : base(unitOfWork) 
        {
        
        }
    }
}
