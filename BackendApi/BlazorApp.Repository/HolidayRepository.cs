﻿using BlazorApp.Entities;
using BlazorApp.Repository.Interface;

namespace BlazorApp.Repository
{
    public class HolidayRepository : Repository<Holiday>, IHolidayRepository
    {
        public HolidayRepository(IUnitOfWork unitOfWork) : base(unitOfWork) 
        {
        
        }
    }
}
