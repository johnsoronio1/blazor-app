﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Models;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Repository.Extensions
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class QueryableExtensions
    {
        public static async Task<PaginatedList<T>> ToPaginatedList<T>(this IQueryable<T> query, int page, int pageSize) where T : class
        {
            var result = new PaginatedList<T>();

            result.PageNumber = page;
            result.PageSize = pageSize;
            result.TotalRows = await query.CountAsync();

            var pageCount = (double)result.TotalRows / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = await query.Skip(skip).Take(pageSize).ToListAsync();

            return result;
        }
    }
}
