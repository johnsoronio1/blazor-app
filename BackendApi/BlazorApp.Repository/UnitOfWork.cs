﻿using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Entities.Context;
using BlazorApp.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PayrollDbContext _context;
        private bool disposed = false;

        public UnitOfWork(PayrollDbContext context)
        {
            _context = context;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Rollback()
        {
            _context.ChangeTracker.Entries().ToList().ForEach(x => x.Reload());
        }

        public DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return _context.Set<TEntity>();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
