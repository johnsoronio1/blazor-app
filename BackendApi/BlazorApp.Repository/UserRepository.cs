﻿using System.Threading.Tasks;
using BlazorApp.Entities;
using BlazorApp.Repository.Interface;

namespace BlazorApp.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IUnitOfWork unitOfWork) : base(unitOfWork) 
        {
        
        }
    }
}
