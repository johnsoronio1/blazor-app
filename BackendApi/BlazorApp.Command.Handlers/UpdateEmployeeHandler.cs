﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Extensions;
using BlazorApp.Models.Middleware.Exceptions;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Models.Requests;
using BlazorApp.Repository.Interface;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace BlazorApp.Command.Handlers
{
    public class UpdateEmployeeHandler : IRequestHandler<UpdateEmployeeRequest, EmployeeDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UpdateEmployeeHandler(
            IMapper mapper,
            IRepository<Employee> employeeRepository,
            IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<EmployeeDto> Handle(UpdateEmployeeRequest request, CancellationToken cancellationToken)
        {
            var employeeEntity = await _employeeRepository.FirstOrDefaultAsync(m => m.EmployeeId == request.IdFromRoute);
            if (employeeEntity == null)
            {
                ApiError error = new ApiError();
                error.Title = "Employee ID Not Found";
                error.ErrorMessage = "Employee ID is not found.";

                throw new NotFoundException(error);
            }

            employeeEntity.DateUpdatedBy = _httpContextAccessor.GetUserId();
            employeeEntity.DateUpdated = DateTime.UtcNow;

            employeeEntity = await _employeeRepository.UpdateAsync(employeeEntity);

            var resultDto = _mapper.Map<Employee, EmployeeDto>(employeeEntity);

            return resultDto;
        }
    }
}
