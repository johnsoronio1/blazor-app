﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Extensions;
using BlazorApp.Models.Requests;
using BlazorApp.Repository.Interface;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace BlazorApp.Command.Handlers
{
    public class CreateEmployeeHandler : IRequestHandler<CreateEmployeeRequest, EmployeeDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CreateEmployeeHandler(
            IMapper mapper,
            IRepository<Employee> employeeRepository,
            IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<EmployeeDto> Handle(CreateEmployeeRequest request, CancellationToken cancellationToken)
        {
            var employeeEntity = _mapper.Map<Employee>(request);

            employeeEntity.DateCreatedBy = _httpContextAccessor.GetUserId();
            employeeEntity.DateCreated = DateTime.UtcNow;
            employeeEntity.DateUpdatedBy = _httpContextAccessor.GetUserId();
            employeeEntity.DateUpdated = DateTime.UtcNow;

            employeeEntity = await _employeeRepository.CreateAsync(employeeEntity);

            var resultDto = _mapper.Map<Employee, EmployeeDto>(employeeEntity);

            return resultDto;
        }
    }
}
