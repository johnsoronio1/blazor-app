﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Exceptions;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Models.Requests;
using BlazorApp.Repository.Interface;
using MediatR;

namespace BlazorApp.Command.Handlers
{
    public class DeleteEmployeeHandler : IRequestHandler<DeleteEmployeeRequest, EmployeeDto>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Employee> _employeeRepository;

        public DeleteEmployeeHandler(
            IMapper mapper,
            IRepository<Employee> employeeRepository)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public async Task<EmployeeDto> Handle(DeleteEmployeeRequest request, CancellationToken cancellationToken)
        {
            var employeeEntity = await _employeeRepository.FirstOrDefaultAsync(m => m.EmployeeId == request.IdFromRoute);
            if (employeeEntity == null)
            {
                ApiError error = new ApiError();
                error.Title = "Employee ID Not Found";
                error.ErrorMessage = "Employee ID is not found.";

                throw new NotFoundException(error);
            }

            await _employeeRepository.DeleteAsync(employeeEntity);

            var resultDto = _mapper.Map<Employee, EmployeeDto>(employeeEntity);

            return resultDto;
        }
    }
}
