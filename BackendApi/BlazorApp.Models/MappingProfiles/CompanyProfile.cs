﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Company
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class CompanyProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyProfile" /> class.
        /// </summary>
        public CompanyProfile()
        {
            //Mapping from Company to CompanyDto
            CreateMap<Company, CompanyDto>();
        }
    }
}
