﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Address
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeAddressProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeAddressProfile" /> class.
        /// </summary>
        public EmployeeAddressProfile()
        {
            //Mapping from EmployeeAddress to EmployeeAddressDto
            CreateMap<EmployeeAddress, EmployeeAddressDto>();
        }
    }
}
