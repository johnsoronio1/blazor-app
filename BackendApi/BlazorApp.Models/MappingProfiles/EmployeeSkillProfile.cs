﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Skill
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeSkillProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeSkillProfile" /> class.
        /// </summary>
        public EmployeeSkillProfile()
        {
            //Mapping from EmployeeSkill to EmployeeSkillDto
            CreateMap<EmployeeSkill, EmployeeSkillDto>();
        }
    }
}
