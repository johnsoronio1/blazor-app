﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Attendance
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class AttendanceProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttendanceProfile" /> class.
        /// </summary>
        public AttendanceProfile()
        {
            //Mapping from Attendance to AttendanceDto
            CreateMap<Attendance, AttendanceDto>().ReverseMap();
        }
    }
}
