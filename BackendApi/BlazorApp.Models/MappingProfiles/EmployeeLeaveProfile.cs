﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Leave
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeLeaveProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeLeaveProfile" /> class.
        /// </summary>
        public EmployeeLeaveProfile()
        {
            //Mapping from EmployeeLeave to EmployeeLeaveDto
            CreateMap<EmployeeLeave, EmployeeLeaveDto>();
        }
    }
}
