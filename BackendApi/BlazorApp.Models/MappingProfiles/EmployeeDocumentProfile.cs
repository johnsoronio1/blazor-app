﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Attendance
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeDocumentProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeDocumentProfile" /> class.
        /// </summary>
        public EmployeeDocumentProfile()
        {
            //Mapping from EmployeeDocument to EmployeeDocumentDto
            CreateMap<EmployeeDocument, EmployeeDocumentDto>().ReverseMap();
        }
    }
}
