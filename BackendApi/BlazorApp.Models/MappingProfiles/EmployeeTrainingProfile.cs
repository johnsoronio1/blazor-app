﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Training
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeTrainingProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeTrainingProfile" /> class.
        /// </summary>
        public EmployeeTrainingProfile()
        {
            //Mapping from EmployeeTraining to EmployeeTrainingDto
            CreateMap<EmployeeTraining, EmployeeTrainingDto>();
        }
    }
}
