﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Education
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeEducationProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeEducationProfile" /> class.
        /// </summary>
        public EmployeeEducationProfile()
        {
            //Mapping from EmployeeEducation to EmployeeEducationDto
            CreateMap<EmployeeEducation, EmployeeEducationDto>();
        }
    }
}
