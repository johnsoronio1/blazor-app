﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    public class MappingProfile : MapperConfigurationExpression
    {
        /// <summary>
        /// Create automap mapping profiles
        /// </summary>
        public MappingProfile()
        {
            CreateMap<GroupManager, GroupManagerDto>().ReverseMap();
            CreateMap<GroupMember, GroupMemberDto>().ReverseMap();
            CreateMap<Holiday, HolidayDto>().ReverseMap();
            CreateMap<HolidayEvent, HolidayEventDto>().ReverseMap();
            CreateMap<Leave, LeaveDto>().ReverseMap();
            CreateMap<LibraryItem, LibraryItemDto>().ReverseMap();
            CreateMap<LibraryManager, LibraryManagerDto>().ReverseMap();
            CreateMap<Login, LoginDto>().ReverseMap();
            CreateMap<Logs, LogDto>().ReverseMap();
            CreateMap<Role, RoleDto>().ReverseMap();
            CreateMap<Tenant, TenantDto>().ReverseMap();
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<UserRole, UserRoleDto>().ReverseMap();
            CreateMap<UserSession, UserSessionDto>().ReverseMap();
            CreateMap<UserToken, UserTokenDto>().ReverseMap();
            CreateMap<WorkShift, WorkShiftDto>().ReverseMap();
            CreateMap<ApiLogItem, ApiLogItemDto>().ReverseMap();
        }
    }
}
