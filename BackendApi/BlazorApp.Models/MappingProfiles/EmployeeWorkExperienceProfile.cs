﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee WorkExperience
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeWorkExperienceProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeWorkExperienceProfile" /> class.
        /// </summary>
        public EmployeeWorkExperienceProfile()
        {
            //Mapping from EmployeeWorkExperience to EmployeeWorkExperienceDto
            CreateMap<EmployeeWorkExperience, EmployeeWorkExperienceDto>();
        }
    }
}
