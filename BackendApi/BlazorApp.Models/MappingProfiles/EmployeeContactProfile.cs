﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Contact
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeContactProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeContactProfile" /> class.
        /// </summary>
        public EmployeeContactProfile()
        {
            //Mapping from EmployeeContact to EmployeeContactDto
            CreateMap<EmployeeContact, EmployeeContactDto>().ReverseMap();
        }
    }
}
