﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Bank
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeBankProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeBankProfile" /> class.
        /// </summary>
        public EmployeeBankProfile()
        {
            //Mapping from EmployeeBank to EmployeeBankDto
            CreateMap<EmployeeBank, EmployeeBankDto>();
        }
    }
}
