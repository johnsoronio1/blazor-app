﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Responses;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee Shift
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeShiftProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeShiftProfile" /> class.
        /// </summary>
        public EmployeeShiftProfile()
        {
            //Mapping from EmployeeShift to EmployeeShiftDto
            CreateMap<EmployeeShift, EmployeeShiftDto>();
        }
    }
}
