﻿using AutoMapper;
using BlazorApp.Entities;
using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Requests;

namespace BlazorApp.Models.MappingProfiles
{
    /// <summary>
    /// The Automapper Profile for Employee
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class EmployeeProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeProfile" /> class.
        /// </summary>
        public EmployeeProfile()
        {
            //Mapping from Employee to EmployeeDto
            CreateMap<Employee, EmployeeDto>();

            //Mapping from CreateEmployeeDto to CreateEmployeeRequest
            CreateMap<CreateEmployeeDto, CreateEmployeeRequest>();

            //Mapping from CreateEmployeeRequest to Employee
            CreateMap<CreateEmployeeRequest, Employee>();

            //Mapping from UpdateEmployeeDto to UpdateEmployeeRequest
            CreateMap<UpdateEmployeeDto, UpdateEmployeeRequest>();

            //Mapping from UpdateEmployeeRequest to Employee
            CreateMap<UpdateEmployeeRequest, Employee>();

            //Mapping from PagingDto to GetEmployeeCollectionRequest
            CreateMap<PagingDto, GetEmployeeCollectionRequest>();

            //Mapping from PaginatedList<Employee> to PaginatedList<EmployeeDto>
            CreateMap<PaginatedList<Employee>, PaginatedList<EmployeeDto>>();
        }
    }
}
