﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class ApiLogItemDto : BaseModel
    {
        public long Id { get; set; }
        public DateTime RequestTime { get; set; }
        public long ResponseMillis { get; set; }
        public int StatusCode { get; set; }
        public string Method { get; set; }
        public string Path { get; set; }
        public string QueryString { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public string IPAddress { get; set; }
        public int UserId { get; set; }
    }
}
