﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class AttendanceDto : BaseModel
    {
        public int AttendanceId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public bool? IsRestDay { get; set; }
        public bool? IsHoliday { get; set; }
        public bool? IsLeave { get; set; }
        public TimeSpan? TimeIn { get; set; }
        public TimeSpan? TimeOut { get; set; }
        public decimal? TotalWorkHr { get; set; }
        public decimal? TotalLateHr { get; set; }
        public decimal? TotalUndertimeHr { get; set; }
        public decimal? TotalOvertimeHr { get; set; }
    }
}
