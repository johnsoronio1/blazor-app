﻿using System.Collections.Generic;

namespace BlazorApp.Models.Dtos.Responses
{
    public class UserSessionDto : BaseModel
    {
        public bool IsAuthenticated { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public int TenantId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> Roles { get; set; }
        public List<KeyValuePair<string, string>> ExposedClaims { get; set; }
        public bool DisableTenantFilter { get; set; }
    }
}
