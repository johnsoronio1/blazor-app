﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeSkillDto : BaseModel
    {
        public int EmpSkill { get; set; }
        public int EmployeeId { get; set; }
        public string Skill { get; set; }
        public string SkillDescription { get; set; }
        public string SkillLevel { get; set; }
        public int? YearExperience { get; set; }
        public bool? Active { get; set; }
    }
}
