﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeEducationDto : BaseModel
    {
        public int EmployeeId { get; set; }
        public string EducationType { get; set; }
        public string EducationLevel { get; set; }
        public string SchoolName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public DateTime? YearStarted { get; set; }
        public DateTime? YearCompleted { get; set; }
    }
}
