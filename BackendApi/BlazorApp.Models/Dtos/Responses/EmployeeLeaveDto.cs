﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeLeaveDto : BaseModel
    {
        public int EmployeeLeaveId { get; set; }
        public int LeaveId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime LeaveStartDate { get; set; }
        public DateTime LeaveEndDate { get; set; }
        public bool? HalfDay { get; set; }
        public string LeaveReason { get; set; }
        public string LeaveStatus { get; set; }
        public bool? Active { get; set; }
    }
}
