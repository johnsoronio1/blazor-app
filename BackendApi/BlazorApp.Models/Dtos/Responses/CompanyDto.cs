﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class CompanyDto: BaseModel
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyDescription { get; set; }
        public string CompanyAddress { get; set; }
        public int? Active { get; set; }
    }
}
