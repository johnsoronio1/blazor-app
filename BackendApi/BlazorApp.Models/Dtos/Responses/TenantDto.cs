﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class TenantDto : BaseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
