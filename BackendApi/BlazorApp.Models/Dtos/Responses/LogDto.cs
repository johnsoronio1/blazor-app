﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class LogDto : BaseModel
    {
        public int LogId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }
        public DateTime? TimeIn { get; set; }
        public DateTime? TimeOut { get; set; }
    }
}
