﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeContactDto : BaseModel
    {
        public int EmpContactId { get; set; }
        public int EmployeeId { get; set; }
        public string ContactType { get; set; }
        public string ContactNumber { get; set; }
        public bool? IsCurrent { get; set; }
        public bool? Active { get; set; }
    }
}
