﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeAddressDto : BaseModel
    {
        public int EmpAddressId { get; set; }
        public int EmployeeId { get; set; }
        public string AddressType { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int? PostalCode { get; set; }
        public bool? IsCurrentAddress { get; set; }
        public bool? Active { get; set; }
    }
}
