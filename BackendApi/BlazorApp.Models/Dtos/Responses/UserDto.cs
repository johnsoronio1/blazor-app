﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class UserDto : BaseModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string LastLoggedIn { get; set; }
    }
}
