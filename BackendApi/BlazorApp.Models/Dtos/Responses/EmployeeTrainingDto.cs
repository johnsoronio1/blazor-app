﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeTrainingDto : BaseModel
    {
        public int EmpTraining { get; set; }
        public int EmployeeId { get; set; }
        public string TrainingName { get; set; }
        public string TrainingDescription { get; set; }
        public DateTime DateTaken { get; set; }
        public string Venue { get; set; }
        public string Remarks { get; set; }
    }
}
