﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class RoleDto : BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
