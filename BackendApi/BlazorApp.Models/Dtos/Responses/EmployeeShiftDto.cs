﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeShiftDto : BaseModel
    {
        public int EmployeeShiftId { get; set; }
        public int EmployeeId { get; set; }
        public int WorkShiftId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public bool? Active { get; set; }
    }
}
