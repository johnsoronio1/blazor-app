﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class LibraryManagerDto : BaseModel
    {
        public int LibraryId { get; set; }
        public string LibraryName { get; set; }
        public string LibraryDescription { get; set; }
        public int? Active { get; set; }
    }
}
