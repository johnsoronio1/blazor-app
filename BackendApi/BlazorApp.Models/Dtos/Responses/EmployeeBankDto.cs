﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeBankDto : BaseModel
    {
        public int EmployeeBankId { get; set; }
        public int EmployeeId { get; set; }
        public string BankName { get; set; }
        public string BankDescription { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public bool? IsCurrent { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Currency { get; set; }
        public bool? Active { get; set; }
    }
}
