﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeDocumentDto : BaseModel
    {
        public int EmpDocument { get; set; }
        public int EmployeeId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentName { get; set; }
        public DateTime? DocumentUploaded { get; set; }
        public string Remarks { get; set; }
    }
}
