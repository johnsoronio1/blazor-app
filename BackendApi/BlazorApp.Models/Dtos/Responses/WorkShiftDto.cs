﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class WorkShiftDto : BaseModel
    {
        public int WorkShiftId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
        public DateTime? BreakStart { get; set; }
        public DateTime? BreakEnd { get; set; }
        public decimal? BreakHr { get; set; }
        public decimal? DailyWorkHr { get; set; }
        public decimal? LateHr { get; set; }
        public bool? AssumePresent { get; set; }
        public bool? Flexible { get; set; }
        public bool? Monday { get; set; }
        public bool? Tuesday { get; set; }
        public bool? Wednesday { get; set; }
        public bool? Thursday { get; set; }
        public bool? Friday { get; set; }
        public bool? Saturday { get; set; }
        public bool? Sunday { get; set; }
        public bool? Active { get; set; }
    }
}
