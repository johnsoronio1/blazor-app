﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class HolidayDto : BaseModel
    {
        public int HolidayId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string HolidayType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? Yearly { get; set; }
        public bool? Active { get; set; }
    }
}
