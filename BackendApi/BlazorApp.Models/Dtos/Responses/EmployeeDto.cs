﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeDto : BaseModel
    {
        public int EmployeeId { get; set; }
        public int? EmployeeCompanyId { get; set; }
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PrefixName { get; set; }
        public string SuffixName { get; set; }
        public int? Age { get; set; }
        public string Gender { get; set; }
        public string Active { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? HiredDate { get; set; }
    }
}
