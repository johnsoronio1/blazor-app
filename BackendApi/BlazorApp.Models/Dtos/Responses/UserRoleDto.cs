﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class UserRoleDto : BaseModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
