﻿namespace BlazorApp.Models.Dtos.Responses
{
    /// <summary>
    /// The User Logged Dto. 
    /// </summary>
    public class UserLoggedDto
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The User Identifier
        /// </value>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>
        /// The Display Name
        /// </value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>
        /// The Access Token
        /// </value>
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the refresh token.
        /// </summary>
        /// <value>
        /// The Refresh Token
        /// </value>
        public string RefreshToken { get; set; }
    }
}
