﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class EmployeeWorkExperienceDto : BaseModel
    {
        public int EmpExperience { get; set; }
        public int EmployeeId { get; set; }
        public string Position { get; set; }
        public decimal? BasicPay { get; set; }
        public decimal? Allowance { get; set; }
        public string Company { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public DateTime? DateStarted { get; set; }
        public DateTime? DateEnded { get; set; }
        public string ReasonLeave { get; set; }
        public string OtherReason { get; set; }
    }
}
