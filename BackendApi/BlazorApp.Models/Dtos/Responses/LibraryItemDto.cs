﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class LibraryItemDto : BaseModel
    {
        public int LibraryItemId { get; set; }
        public int LibManagerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public bool? Active { get; set; }
    }
}
