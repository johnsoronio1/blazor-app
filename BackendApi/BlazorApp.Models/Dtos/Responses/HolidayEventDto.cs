﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class HolidayEventDto : BaseModel
    {
        public int HolidayEventId { get; set; }
        public int HolidayId { get; set; }
        public int GroupId { get; set; }
        public bool? Active { get; set; }
    }
}
