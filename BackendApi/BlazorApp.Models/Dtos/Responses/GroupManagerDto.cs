﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class GroupManagerDto : BaseModel
    {
        public int GroupId { get; set; }
        public string GroupType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? Active { get; set; }
    }
}
