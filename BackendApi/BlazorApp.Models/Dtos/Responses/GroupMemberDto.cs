﻿namespace BlazorApp.Models.Dtos.Responses
{
    public class GroupMemberDto : BaseModel
    {
        public int GroupMemberId { get; set; }
        public int GroupId { get; set; }
        public int EmployeeId { get; set; }
        public bool? Active { get; set; }
    }
}
