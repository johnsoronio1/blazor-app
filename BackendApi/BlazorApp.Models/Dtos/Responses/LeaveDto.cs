﻿using System;

namespace BlazorApp.Models.Dtos.Responses
{
    public class LeaveDto : BaseModel
    {
        public int LeaveId { get; set; }
        public string LeaveType { get; set; }
        public string LeaveName { get; set; }
        public string LeaveDescription { get; set; }
        public decimal? Credits { get; set; }
        public bool? Yearly { get; set; }
        public DateTime? RefreshDate { get; set; }
        public decimal? MaxCashConvert { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool? Active { get; set; }
    }
}
