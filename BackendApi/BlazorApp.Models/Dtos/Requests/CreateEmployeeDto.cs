﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlazorApp.Models.Dtos.Requests
{
    public class CreateEmployeeDto
    {
        public int? EmployeeCompanyId { get; set; }
        public int? UserId { get; set; }
        [Required]
        [StringLength(60)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(60)]
        public string MiddleName { get; set; }
        [Required]
        [StringLength(60)]
        public string LastName { get; set; }
        [StringLength(30)]
        public string PrefixName { get; set; }
        [StringLength(30)]
        public string SuffixName { get; set; }
        [Required]
        public int? Age { get; set; }
        [Required]
        [StringLength(10)]
        public string Gender { get; set; }
        public string Active { get; set; }
        [Required]
        public DateTime? BirthDate { get; set; }
        [Required]
        public DateTime? HiredDate { get; set; }
    }
}
