﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp.Models.Dtos.Requests
{
    public class UserEntry
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public string DisplayName { get; set; }

        public string RoleType { get; set; }
    }
}
