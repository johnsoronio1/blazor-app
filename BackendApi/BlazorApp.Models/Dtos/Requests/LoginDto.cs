﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlazorApp.Models.Dtos.Requests
{
    public class LoginDto : BaseModel
    {
        public int LoginId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        [StringLength(60)]
        public string Username { get; set; }
        [Required]
        [StringLength(150)]
        public string Password { get; set; }
        [Required]
        [StringLength(100)]
        public string EmailAddress { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateLastLogin { get; set; }
    }
}
