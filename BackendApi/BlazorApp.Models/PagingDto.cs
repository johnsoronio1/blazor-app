﻿using System.Collections.Generic;

namespace BlazorApp.Models
{
    public class PagingDto
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SortField { get; set; }
        public bool SortByDesc { get; set; }
        public List<PaginatedFilters> Filters { get; set; }
    }

    public class PaginatedFilters
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
