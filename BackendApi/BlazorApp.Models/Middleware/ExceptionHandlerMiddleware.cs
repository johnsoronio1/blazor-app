﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using BlazorApp.Models.Middleware.Exceptions;
using BlazorApp.Models.Middleware.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace BlazorApp.Models.Middleware
{
    /// <summary>
    /// The exception handler middleware.
    /// </summary>
    public class ExceptionHandlerMiddleware
    {
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionHandlerMiddleware" /> class.
        /// </summary>
        /// <param name="next">The next.</param>
        /// <param name="logger">The logger service.</param>
        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// Invokes the asynchronous.
        /// </summary>
        /// <param name="context">The context.</param>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, System.Exception ex)
        {
            var result = new ApiResponse
            {
                Title = "Internal Server Error",
                ErrorMessage = ex.Message,
                ReferenceCode = ex.GetType().ToString(),
                ReferenceDocumentLink = GetRoute(context.GetRouteData()),
            };

            if (ex is BadRequestException badinput)
            {
                result.Title = badinput.Title;
                result.ErrorCode = StatusCodes.Status400BadRequest;

                _logger.LogDebug(ex, badinput.Message);
            }
            else if (ex is NotFoundException notfound)
            {
                result.Title = notfound.Title;
                result.ErrorCode = StatusCodes.Status404NotFound;

                _logger.LogDebug(ex, notfound.Message);
            }
            else if (ex is UnauthorizedAccessException unauthorizedAccessException)
            {
                result.Title = "Unauthorized access.";
                result.ErrorCode = StatusCodes.Status401Unauthorized;

                _logger.LogDebug(ex, unauthorizedAccessException.Message);
            }
            else if (ex is TimeoutException timeout)
            {
                result.ErrorMessage = "Database connection timeout.";
                result.ErrorCode = StatusCodes.Status500InternalServerError;

                _logger.LogError(ex, timeout.Message);
            }
            else
            {
                result.ErrorCode = StatusCodes.Status500InternalServerError;
                _logger.LogError(ex, ex.Message);
            }

            var serializeOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
            };

            // write the response
            context.Response.StatusCode = result.ErrorCode;
            context.Response.ContentType = "application/json";

            await context.Response.WriteAsync(JsonSerializer.Serialize(result, serializeOptions));
        }

        private string GetRoute(RouteData routeData)
        {
            return $"{routeData.Values["controller"]}/{routeData.Values["action"]}";
        }
    }
}
