﻿using BlazorApp.Models.Middleware.Wrappers;

namespace BlazorApp.Models.Middleware.Exceptions
{
    /// <summary>
    /// The not found exception
    /// </summary>
    /// <seealso cref="BaseException" />
    public class NotFoundException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundException"/> class.
        /// </summary>
        /// <param name="errorInfo">The error info.</param>
        public NotFoundException(ApiError apiError)
            : base(apiError)
        {

        }
    }
}
