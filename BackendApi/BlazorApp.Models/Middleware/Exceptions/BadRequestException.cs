﻿using BlazorApp.Models.Middleware.Wrappers;

namespace BlazorApp.Models.Middleware.Exceptions
{
    /// <summary>
    /// Bad request exception
    /// </summary>
    /// <seealso cref="BlazorApp.Models.Middleware.Exceptions.BaseException" />
    public class BadRequestException : BaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BadRequestException"/> class.
        /// </summary>
        /// <param name="errorInfo">The error info.</param>
        public BadRequestException(ApiError apiError)
            : base(apiError)
        {
        }
    }
}
