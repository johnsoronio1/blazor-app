﻿using System;
using BlazorApp.Models.Middleware.Wrappers;

namespace BlazorApp.Models.Middleware.Exceptions
{
    /// <summary>
    /// The base exception.
    /// </summary>
    /// <seealso cref="Exception" />
    public abstract class BaseException : Exception
    {
        /// <summary>
        /// Gets or set the title.
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        public int ErrorCode { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseException"/> class.
        /// </summary>
        /// <param name="errorInfo">The error info.</param>
        public BaseException(ApiError errorInfo)
            : base(errorInfo.ErrorMessage)
        {
            Title = errorInfo.Title;
            ErrorCode = (int)errorInfo.ErrorCode;
        }
    }
}
