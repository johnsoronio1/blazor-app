﻿namespace BlazorApp.Models.Middleware.Wrappers
{
    /// <summary>
    /// The api error details.
    /// </summary>
    public class ApiError
    {
        /// <summary>
        /// Gets or sets the error title.
        /// </summary>
        /// <value>
        /// The error title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the error details.
        /// </summary>
        /// <value>
        /// The error details.
        /// </value>
        public string Details { get; set; }
    }
}
