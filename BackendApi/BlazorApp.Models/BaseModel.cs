﻿using System;
using System.Collections.Generic;
using System.Text;
using BlazorApp.Models.Interface;

namespace BlazorApp.Models
{
    public class BaseModel : IBaseModel
    {
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }
    }
}
