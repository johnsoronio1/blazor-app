﻿using System.Collections.Generic;
using BlazorApp.Models.Dtos.Responses;
using MediatR;

namespace BlazorApp.Models.Requests
{
    /// <summary>
    /// The Get Employee Collection Request
    /// </summary>
    /// <seealso cref="BlazorApp.Models.PagingDto" />
    /// <seealso cref="MediatR.IRequest&lt;System.Collections.Generic.IEnumerable&lt;BlazorApp.Models.PaginatedList&lt;BlazorApp.Models.Dtos.Responses.EmployeeDto&gt;&gt;" />
    public class GetEmployeeCollectionRequest : PagingDto, IRequest<PaginatedList<EmployeeDto>>
    {

    }
}
