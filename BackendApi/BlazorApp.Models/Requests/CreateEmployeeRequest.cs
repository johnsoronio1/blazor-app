﻿using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;
using MediatR;

namespace BlazorApp.Models.Requests
{
    /// <summary>
    /// The Create Employee Request
    /// </summary>
    /// <seealso cref="BlazorApp.Models.Dtos.Requests.CreateEmployeeDto" />
    /// <seealso cref="MediatR.IRequest&lt;BlazorApp.Models.Dtos.Responses.EmployeeDto&gt;" />
    public class CreateEmployeeRequest : CreateEmployeeDto, IRequest<EmployeeDto>
    {

    }
}
