﻿using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;
using MediatR;

namespace BlazorApp.Models.Requests
{
    /// <summary>
    /// The Update Employee Request
    /// </summary>
    /// <seealso cref="BlazorApp.Models.Dtos.Requests.UpdateEmployeeDto" />
    /// <seealso cref="MediatR.IRequest&lt;BlazorApp.Models.Dtos.Responses.EmployeeDto&gt;" />
    public class UpdateEmployeeRequest : UpdateEmployeeDto, IRequest<EmployeeDto>
    {
        /// <summary>
        /// Gets or sets the chat room id from route.
        /// </summary>
        /// <value>
        /// The chat room id from route.
        /// </value>
        public int IdFromRoute { get; set; }
    }
}
