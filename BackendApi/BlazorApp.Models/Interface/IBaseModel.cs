﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorApp.Models.Interface
{
    public interface IBaseModel
    {
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }
    }
}
