﻿using System;
using IdentityModel;
using Microsoft.AspNetCore.Http;

namespace BlazorApp.Models.Extensions
{
    public static class IHttpContextAccessorExtension
    {
        /// <summary>
        /// Gets the username.
        /// </summary>
        /// <param name="httpContextAccessor">The http context accessor.</param>
        /// <returns>The username.</returns>
        public static string GetUsername(this IHttpContextAccessor httpContextAccessor)
        {
            var username = httpContextAccessor.HttpContext.User.FindFirst(x => x.Type == JwtClaimTypes.Profile);
            return username != null ? username.Value : null;
        }

        /// <summary>
        /// Gets the user email.
        /// </summary>
        /// <param name="httpContextAccessor">The http context accessor.</param>
        /// <returns>The user email.</returns>
        public static string GetUserEmail(this IHttpContextAccessor httpContextAccessor)
        {
            var email = httpContextAccessor.HttpContext.User.FindFirst(x => x.Type == JwtClaimTypes.Email);
            return email != null ? email.Value : null;
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <param name="httpContextAccessor">The http context accessor.</param>
        /// <returns>The user id.</returns>
        public static int? GetUserId(this IHttpContextAccessor httpContextAccessor)
        {
            var userId = httpContextAccessor.HttpContext.User.FindFirst(x => x.Type == JwtClaimTypes.Subject);
            if (userId != null)
                return null;
            else
                return Convert.ToInt32(userId);
        }

        /// <summary>
        /// Gets the user role.
        /// </summary>
        /// <param name="httpContextAccessor">The http context accessor.</param>
        /// <returns>The role.</returns>
        public static string GetUserRole(this IHttpContextAccessor httpContextAccessor)
        {
            var role = httpContextAccessor.HttpContext.User.FindFirst(x => x.Type == JwtClaimTypes.Role);
            return role != null ? role.Value : null;
        }
    }
}
