﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using BlazorApp.Command.Handlers;
using BlazorApp.Filters;
using BlazorApp.Models.MappingProfiles;
using BlazorApp.Models.Middleware.Extensions;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Query.Handlers;
using BlazorApp.Repository;
using BlazorApp.Repository.Interface;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace BlazorApp.Extensions
{
    /// <summary>
    /// The service collection extension.
    /// </summary>
    public static class IServiceCollectionExtension
    {
        /// <summary>
        /// Adds the settings.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        public static void AddSettings(this IServiceCollection services, IConfiguration configuration)
        {

        }

        /// <summary>
        /// Adds the services setup.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddMvc().ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = (context) =>
                {
                    var result = new ApiResponse
                    {
                        Title = "The request is invalid.",
                        ErrorCode = (int)HttpStatusCode.BadRequest,
                        ErrorMessage = "One or more validation errors occurred.",
                        Errors = context.ModelState.AllErrors(),
                        ReferenceCode = "BlazorApp.Models.Middleware.Exceptions.BadRequestException",
                        ReferenceDocumentLink = GetRoute(context.HttpContext.GetRouteData())
                    };
                    return new BadRequestObjectResult(result);
                };
            });
            services.AddControllers();
            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
        }

        /// <summary>
        /// Adds the repositories.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration"></param>
        public static void AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IAttendanceRepository, AttendanceRepository>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IEmployeeLogRepository, EmployeeLogRepository>();
            services.AddScoped<IWorkShiftRepository, WorkShiftRepository>();
            services.AddScoped<IHolidayRepository, HolidayRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
        }

        /// <summary>
        /// Adds the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void AddExternalService(this IServiceCollection services)
        {
           
        }

        /// <summary>
        /// Adds the automatic mapper profiles.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void AddAutoMapperProfiles(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(AttendanceProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeAddressProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeBankProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeContactProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeDocumentProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeEducationProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeLeaveProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeShiftProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeSkillProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeTrainingProfile).Assembly);
            services.AddAutoMapper(typeof(EmployeeWorkExperienceProfile).Assembly);
            services.AddAutoMapper(typeof(CompanyProfile).Assembly);
            services.AddAutoMapper(typeof(MappingProfile).Assembly);
        }

        /// <summary>
        /// Adds media r handlers.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void AddMediaRHandlers(this IServiceCollection services)
        {
            services.AddMediatR(typeof(CreateEmployeeHandler));
            services.AddMediatR(typeof(UpdateEmployeeHandler));
            services.AddMediatR(typeof(DeleteEmployeeHandler));

            services.AddMediatR(typeof(GetEmployeeCollectionHandler));
            services.AddMediatR(typeof(GetEmployeeByIdHandler));
        }

        /// <summary>
        /// Adds custom swagger.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration">The configuration.</param>
        public static void AddCustomSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new OpenApiInfo { Title = "You api title", Version = "v1" });
                config.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                config.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });

                config.OperationFilter<AuthorizeCheckOperationFilter>();

                // make sure generate swagger with operation id
                config.CustomOperationIds(e => $"{e.ActionDescriptor.RouteValues["action"]}");

                // xml document for decorate the swagger
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                config.IncludeXmlComments(xmlPath);
            });
        }

        private static string GetRoute(RouteData routeData)
        {
            return $"{routeData.Values["controller"]}/{routeData.Values["action"]}";
        }
    }
}
