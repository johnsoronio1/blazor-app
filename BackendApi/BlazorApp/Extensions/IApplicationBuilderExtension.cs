﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace BlazorApp.Extensions
{
    /// <summary>
    /// The IApplication builder extension.
    /// </summary>
    public static class IApplicationBuilderExtension
    {
        /// <summary>
        /// Uses custom swagger.
        /// </summary>
        /// <param name="app">The application builder.</param>        
        /// <param name="configuration"></param>
        public static void UseCustomSwagger(this IApplicationBuilder app, IConfiguration configuration)
        {
            app.UseSwagger(options =>
            {
                options.SerializeAsV2 = true;
                options.RouteTemplate = "BlazorApp/api/swagger/{documentName}/swagger.json";
            });

            app.UseSwaggerUI(config =>
            {
                config.DocumentTitle = "Swagger UI";
                config.SwaggerEndpoint("/BlazorApp/api/swagger/v1/swagger.json", $"BlazorApp V1");
                config.RoutePrefix = "BlazorApp/api/swagger";
                config.OAuthClientSecret("no_password");
            });
        }
    }
}
