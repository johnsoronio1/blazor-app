﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Repository.Interface;
using BlazorApp.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The Account Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class AccountsController : ControllerBase
    {
        private readonly ILogger<AccountsController> _logger;
        private readonly ITokenStoreService _tokenStoreService;
        private readonly IUnitOfWork _uow;
        private readonly IAntiForgeryCookieService _antiforgery;
        private readonly ITokenFactoryService _tokenFactoryService;
        private readonly IUsersService _userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsController"/> class.
        /// </summary>
        /// <param name="logger">The logger service.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="tokenStoreService">The token store service.</param>
        /// <param name="tokenFactoryService">The token factory service.</param>
        /// <param name="uow">The unit of work service.</param>
        /// <param name="antiforgery">The anti forgery service.</param>
        public AccountsController(
            ILogger<AccountsController> logger,
            IUsersService userService,
            ITokenStoreService tokenStoreService,
            ITokenFactoryService tokenFactoryService,
            IUnitOfWork uow,
            IAntiForgeryCookieService antiforgery) 
        {
            _logger = logger;
            _userService = userService;
            _tokenStoreService = tokenStoreService;
            _uow = uow;
            _antiforgery = antiforgery;
            _tokenFactoryService = tokenFactoryService;
        }

        /// <summary>
        /// Validates User Login Account.
        /// </summary>
        /// <param name="loginDto">The user login dto.</param>
        /// <returns>The User Detail Dto.</returns>
        [AllowAnonymous]
        [IgnoreAntiforgeryToken]
        [HttpPost("[action]")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(UserLoggedDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(UserLoggedDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> Login([FromBody] UserLoginDto loginDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(loginDto);
            }

            try
            {
                var user = await _userService.FindUserAsync(loginDto.Username, loginDto.Password);
                if (user == null)
                {
                    return NotFound("User Account is not found");
                }

                if (!user.IsActive)
                {
                    return BadRequest("User Account is not active");
                }

                var result = await _tokenFactoryService.CreateJwtTokensAsync(user);
                await _tokenStoreService.AddUserTokenAsync(user, result.RefreshTokenSerial, result.AccessToken, null);
                await _uow.SaveChangesAsync();

                _antiforgery.RegenerateAntiForgeryCookies(result.Claims);

                var responseDto = new UserLoggedDto() 
                { 
                    UserId = user.Id, 
                    DisplayName = user.DisplayName, 
                    AccessToken = result.AccessToken, 
                    RefreshToken = result.RefreshToken 
                };

                return Ok(responseDto);
            }
            catch (Exception ex) 
            {
                _logger.LogInformation("Login Failed: " + ex.Message);
            }

            _logger.LogInformation("Invalid Password for user {0}}", loginDto.Username);

            return BadRequest("Login Failed");
        }

        /// <summary>
        /// Registers new User Account.
        /// </summary>
        /// <param name="createDto">The user entry dto.</param>
        /// <returns>The User Detail Dto.</returns>
        [AllowAnonymous]
        [HttpPost("[action]")]
        [IgnoreAntiforgeryToken]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> Register([FromBody] UserEntry createDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(createDto);
            }

            var existingUser = await _userService.FindUserByUsernameAsync(createDto.Username);
            if (existingUser != null)
            {
                return BadRequest("Username is already taken");
            }

            return Ok(await _userService.CreateAsync(createDto));
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> RefreshToken([FromBody] JToken jsonBody)
        {
            var refreshTokenValue = jsonBody.Value<string>("refreshToken");
            if (string.IsNullOrWhiteSpace(refreshTokenValue))
            {
                return BadRequest("refreshToken is not set.");
            }

            var token = await _tokenStoreService.FindTokenAsync(refreshTokenValue);
            if (token == null)
            {
                return Unauthorized();
            }

            var result = await _tokenFactoryService.CreateJwtTokensAsync(token.User);
            await _tokenStoreService.AddUserTokenAsync(token.User, result.RefreshTokenSerial, result.AccessToken, _tokenFactoryService.GetRefreshTokenSerial(refreshTokenValue));
            await _uow.SaveChangesAsync();

            _antiforgery.RegenerateAntiForgeryCookies(result.Claims);

            return Ok(new { access_token = result.AccessToken, refresh_token = result.RefreshToken });
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Logout(string refreshToken)
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var userIdValue = claimsIdentity.FindFirst(ClaimTypes.UserData)?.Value;

            // The Jwt implementation does not support "revoke OAuth token" (logout) by design.
            // Delete the user's tokens from the database (revoke its bearer token)
            await _tokenStoreService.RevokeUserBearerTokensAsync(userIdValue, refreshToken);
            await _uow.SaveChangesAsync();

            _antiforgery.DeleteAntiForgeryCookies();

            return Ok("Logout successful");
        }

        [Authorize]
        [HttpGet("[action]")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public IActionResult IsAuthenticated()
        {
            return Ok(User.Identity.IsAuthenticated);
        }

        [Authorize]
        [HttpGet("[action]")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public IActionResult GetUserInfo()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            return Ok(new { Username = claimsIdentity.Name });
        }
    }
}