﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The Login Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [Authorize]
    public class LoginsController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginsController"/> class.
        /// </summary>
        public LoginsController()
        {

        }
    }
}