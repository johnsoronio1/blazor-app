﻿using System.Threading.Tasks;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The Holiday Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [Authorize]
    public class HolidaysController : ControllerBase
    {
        private readonly IHolidayService _holidayService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HolidaysController"/> class.
        /// </summary>
        /// <param name="holidayService">The holiday service.</param>
        public HolidaysController(IHolidayService holidayService)
        {
            _holidayService = holidayService;
        }

        /// <summary>
        /// Gets the holiday dto list.
        /// </summary>
        /// <returns>The Holiday Dto List.</returns>
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetHolidayList()
        {
            return Ok(await _holidayService.ListAsync());
        }

        // GET: api/holiday/5
        /// <summary>
        /// Gets the holiday details by id.
        /// </summary>
        /// <param name="id">The holiday identifier.</param>
        /// <returns>The Holiday Detail Dto.</returns>
        [HttpGet("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetHolidayDetailsById(int id)
        {
            return Ok(await _holidayService.GetByIdAsync(id));
        }

        // POST: api/holiday
        /// <summary>
        /// Creates new holiday.
        /// </summary>
        /// <param name="createDto">The create holiday dto.</param>
        /// <returns>The Holiday Detail Dto.</returns>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> CreateHoliday([FromBody] HolidayDto createDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(createDto);
            }

            return Ok(await _holidayService.CreateAsync(createDto));
        }

        // PUT: api/holiday/5
        /// <summary>
        /// Updates existing holiday.
        /// </summary>
        /// <param name="id">The holiday identifier.</param>
        /// <param name="updateDto">The update holiday dto.</param>
        /// <returns>The Holiday Detail Dto.</returns>
        [HttpPut("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(HolidayDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> UpdateHoliday(int id, HolidayDto updateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(updateDto);
            }

            return Ok(await _holidayService.UpdateAsync(id, updateDto));
        }

        // DELETE: api/holiday/5
        /// <summary>
        /// Delete existing holiday by id.
        /// </summary>  
        /// <param name="id">The holiday identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> DeleteHoliday(int id)
        {
            return Ok(await _holidayService.DeleteAsync(id));
        }
    }
}