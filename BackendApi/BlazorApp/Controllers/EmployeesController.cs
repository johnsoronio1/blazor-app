﻿using System.Threading.Tasks;
using AutoMapper;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Models.Requests;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The Employee Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class EmployeesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeesController"/> class.
        /// </summary>
        /// <param name="mediator">The mediator.</param>
        /// <param name="mapper">The mapper. </param>
        public EmployeesController(IMediator mediator, IMapper mapper) 
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the employee dto list.
        /// </summary>
        /// <param name="pagingDto">The pagination params.</param>
        /// <returns>The Attendance Dto List.</returns>
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(PaginatedList<EmployeeDto>))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(PaginatedList<EmployeeDto>))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetEmployeeList([FromQuery] PagingDto pagingDto)
        {
            var request = _mapper.Map<GetEmployeeCollectionRequest>(pagingDto);
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        // GET: api/Employee/5
        /// <summary>
        /// Gets the employee details by id.
        /// </summary>
        /// <param name="id">The employee identifier.</param>
        /// <returns>The Employee Detail Dto.</returns>
        [HttpGet("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetEmployeeDetailsById(int id)
        {
            var request = new GetEmployeeByIdRequest();
            request.IdFromRoute = id;
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        // POST: api/Employee
        /// <summary>
        /// Creates new employee.
        /// </summary>
        /// <param name="createDto">The create employee dto.</param>
        /// <returns>The Employee Detail Dto.</returns>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> CreateEmployee([FromBody] CreateEmployeeDto createDto)
        {
            var request = _mapper.Map<CreateEmployeeRequest>(createDto);
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        // PUT: api/Employee/5
        /// <summary>
        /// Updates existing employee.
        /// </summary>
        /// <param name="id">The employee identifier.</param>
        /// <param name="updateDto">The update employee dto.</param>
        /// <returns>The Employee Detail Dto.</returns>
        [HttpPut("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> UpdateEmployee(int id, UpdateEmployeeDto updateDto)
        {
            var request = _mapper.Map<UpdateEmployeeRequest>(updateDto);
            request.IdFromRoute = id;
            var result = await _mediator.Send(request);

            return Ok(result);
        }


        // DELETE: api/Employee/5
        /// <summary>
        /// Deletes existing employee.
        /// </summary>
        /// <param name="id">The employee identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(EmployeeDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            var request = new DeleteEmployeeRequest();
            request.IdFromRoute = id;
            var result = await _mediator.Send(request);

            return Ok(result);
        }
    }
}