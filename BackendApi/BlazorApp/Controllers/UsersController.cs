﻿using System.Threading.Tasks;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Requests;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The User Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersController"/> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        public UsersController(IUsersService userService) 
        {
            _userService = userService;
        }

        // GET: api/user
        /// <summary>
        /// Gets the user dto list.
        /// </summary>
        /// <returns>The User Dto List.</returns>
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetUserList() 
        {
            return Ok(await _userService.GetAll());
        }

        // GET: api/user/5
        /// <summary>
        /// Gets the user details by id.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns>The User Detail Dto.</returns>
        [HttpGet("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetUserById(int id)
        {
            return Ok(await _userService.GetbyId(id));
        }

        // POST: api/user
        /// <summary>
        /// Creates new user.
        /// </summary>
        /// <param name="createDto">The create user dto.</param>
        /// <returns>The User Detail Dto.</returns>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> CreateUser([FromBody] UserEntry createDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(createDto);
            }

            return Ok(await _userService.CreateAsync(createDto));
        }

        // PUT: api/user/5
        /// <summary>
        /// Updates existing user.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <param name="updateDto">The update user dto.</param>
        /// <returns>The User Detail Dto.</returns>
        [HttpPut("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(UserDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> UpdateUser(int id, UserDto updateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(updateDto);
            }

            return Ok(await _userService.UpdateAsync(id, updateDto));
        }

        // DELETE: api/Survey/5
        /// <summary>
        /// Deletes existing user.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> DeleteUser(int id)
        {
            return Ok(await _userService.DeleteAsync(id));
        }
    }
}