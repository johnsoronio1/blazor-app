﻿using System.Threading.Tasks;
using BlazorApp.Entities.Interface;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The Employee Logs Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmployeeLogsController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeLogService _employeeLogService;
        private readonly IUserSession _userSession;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeLogsController"/> class.
        /// </summary>
        /// <param name="employeeService">The employee service.</param>
        /// <param name="employeeLogService">The employee log service.</param>
        /// <param name="userSession">The user session.</param>
        public EmployeeLogsController(IEmployeeService employeeService, IEmployeeLogService employeeLogService, IUserSession userSession) 
        {
            _employeeService = employeeService;
            _employeeLogService = employeeLogService;
            _userSession = userSession;
        }

        /// <summary>
        /// Gets the employee logs dto list.
        /// </summary>
        /// <returns>Employee Logs Dto List.</returns>
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(LogDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(LogDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetEmployeeLogs()
        {
            var userId = _userSession.UserId;
            var employee = await _employeeService.GetAsync(m => m.UserId == userId);
            var data = await _employeeLogService.ListAsync(m => m.EmployeeId != employee.EmployeeId);

            return Ok(data);
        }

        /// <summary>
        /// Gets the current employee logs dto list.
        /// </summary>
        /// <returns>Employee Logs Dto List.</returns>
        [HttpGet("[action]/Current")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(LogDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(LogDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> Current()
        {
            var userId = _userSession.UserId;
            var employee = await _employeeService.GetAsync(m => m.UserId == userId);
            var data = await _employeeLogService.ListAsync(m => m.EmployeeId == employee.EmployeeId);

            return Ok(data);
        }

        /// <summary>
        /// Creates time-in entry for the logged employee.
        /// </summary>
        /// <returns></returns>
        [HttpPost("[action]/TimeIN")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> TimeIn()
        {
            var userId = _userSession.UserId;
            await _employeeLogService.TimeIn(userId);

            return Ok();
        }

        /// <summary>
        /// Creates time-out entry for the logged employee.
        /// </summary>
        /// <returns></returns>
        [HttpPost("[action]/TimeOUT")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> TimeOut()
        {
            var userId = _userSession.UserId;
            await _employeeLogService.TimeOut(userId);

            return Ok();
        }
    }
}