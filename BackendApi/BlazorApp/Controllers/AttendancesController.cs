﻿using System.Threading.Tasks;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The Attendance Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [Authorize]
    public class AttendancesController : ControllerBase
    {
        private readonly IAttendanceService _attendanceService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttendancesController"/> class.
        /// </summary>
        /// <param name="attendanceService">The attendance service.</param>
        public AttendancesController(IAttendanceService attendanceService)
        {
            _attendanceService = attendanceService;
        }

        /// <summary>
        /// Gets the attendance dto list.
        /// </summary>
        /// <returns>The Attendance Dto List.</returns>
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetAttendanceList()
        {
            var data = await _attendanceService.ListAsync();

            return Ok(data);
        }

        /// <summary>
        /// Gets the attendance details by id.
        /// </summary>
        /// <param name="id">The attendance identifier.</param>
        /// <returns>The Attendance Detail Dto.</returns>
        [HttpGet("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetAttendanceById(int id)
        {
            var data = await _attendanceService.GetByIdAsync(id);

            return Ok(data);
        }

        // POST: api/attendance
        /// <summary>
        /// Creates new attendance.
        /// </summary>
        /// <param name="createDto">The create attendance dto.</param>
        /// <returns>The Attendance Detail Dto.</returns>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> CreateAttendance([FromBody] AttendanceDto createDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(createDto);
            }

            return Ok(await _attendanceService.CreateAsync(createDto));
        }

        // PUT: api/attendance/5
        /// <summary>
        /// Update the attendance by id.
        /// </summary>
        /// <param name="id">The attendance identifier.</param>
        /// <param name="updateDto">The update attendance dto.</param>
        /// <returns>The Attendance Detail Dto.</returns>
        [HttpPut("{id}")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> UpdateAttendace(int id, AttendanceDto updateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(updateDto);
            }

            return Ok(await _attendanceService.UpdateAsync(id, updateDto));
        }

        // DELETE: api/attendance/5
        /// <summary>
        /// Delete the attendance by id.
        /// </summary>
        /// <param name="id">The attendance identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(AttendanceDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<IActionResult> DeleteAttendance(int id)
        {
            return Ok(await _attendanceService.DeleteAsync(id));
        }
    }
}