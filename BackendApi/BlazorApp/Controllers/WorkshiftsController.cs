﻿using System.Threading.Tasks;
using BlazorApp.Models;
using BlazorApp.Models.Dtos.Responses;
using BlazorApp.Models.Middleware.Wrappers;
using BlazorApp.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BlazorApp.Controllers
{
    /// <summary>
    /// The Workshifts Controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("BlazorApp/api/[controller]")]
    [ApiController]
    [Authorize]
    public class WorkshiftsController : ControllerBase
    {
        private readonly IWorkShiftService _workShiftService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkshiftsController"/> class.
        /// </summary>
        /// <param name="workShiftService">The workshift service.</param>
        public WorkshiftsController(IWorkShiftService workShiftService)
        {
            _workShiftService = workShiftService;
        }

        /// <summary>
        /// Gets the workshift dto list.
        /// </summary>
        /// <returns>The Workshift Dto List.</returns>
        [HttpGet]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<ActionResult> GetWorkshiftList()
        {
            return Ok(await _workShiftService.ListAsync());
        }

        // GET: api/workshift/5
        /// <summary>
        /// Gets the workshift details by id.
        /// </summary>
        /// <param name="id">The workshift identifier.</param>
        /// <returns>The Workshift Detail Dto.</returns>
        [HttpGet("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<ActionResult> GetWorkshift(int id)
        {
            return Ok(await _workShiftService.GetByIdAsync(id));
        }

        // POST: api/workshift
        /// <summary>
        /// Creates new workshift.
        /// </summary>
        /// <param name="createDto">The create workshift dto.</param>
        /// <returns>The Workshift Detail Dto.</returns>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<ActionResult> CreateWorkShift([FromBody] WorkShiftDto createDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(createDto);
            }

            return Ok(await _workShiftService.CreateAsync(createDto));
        }


        // PUT: api/workshift/5
        /// <summary>
        /// Updates existing workshift.
        /// </summary>
        /// <param name="id">The workshift identifier.</param>
        /// <param name="updateDto">The update workshift dto.</param>
        /// <returns>The Workshift Detail Dto.</returns>
        [HttpPut("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(WorkShiftDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<ActionResult> UpdateWorkShift(int id, WorkShiftDto updateDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(updateDto);
            }

            return Ok(await _workShiftService.UpdateAsync(id, updateDto));
        }

        // DELETE: api/workshift/5
        /// <summary>
        /// Deletes existing workshift.
        /// </summary>
        /// <param name="id">The workshift identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(bool))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status404NotFound, Type = typeof(ApiResponse))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponse))]
        public async Task<ActionResult> DeleteWorkShift(int id)
        {
            return Ok(await _workShiftService.DeleteAsync(id));
        }
    }
}